<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ExpensesController;
use App\Http\Controllers\OrganisationsController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\TransfersController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', [PublicController::class, 'welcome'])->name('welcome');
Route::get('/manifest.json', [PublicController::class, 'manifest'])->name('manifest');

Route::get('/confirmInvitation/{token}', [ActivityController::class, 'confirmInvitation'])->name('activity.confirmInvitation');
Route::get('/declineInvitation/{token}', [ActivityController::class, 'declineInvitation'])->name('activity.declineInvitation');

Auth::routes();

Route::middleware('auth')->group(function () {
  Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');

  Route::get('/activity', [ActivityController::class, 'index'])->name('activity.index');
  Route::get('/activity/detail/{id}', [ActivityController::class, 'detail'])->name('activity.detail');
  Route::get('/activity/switch/{id}', [ActivityController::class, 'switch'])->name('activity.switch');
  Route::post('/activity/insert', [ActivityController::class, 'insert'])->name('activity.insert');
  Route::post('/activity/inviteUser', [ActivityController::class, 'inviteUser'])->name('activity.inviteUser');

  Route::get('/organisations', [OrganisationsController::class, 'index'])->name('organisations.index');
  Route::get('/expenses', [ExpensesController::class, 'index'])->name('expenses.index');
  Route::get('/expenses/add', [ExpensesController::class, 'add'])->name('expenses.add');
  Route::get('/expenses/detail/{id}', [ExpensesController::class, 'detail'])->name('expenses.detail');
  Route::get('/expenses/dispute/{id}', [ExpensesController::class, 'dispute'])->name('expenses.dispute');
  Route::get('/expenses/confirm/{id}', [ExpensesController::class, 'confirm'])->name('expenses.confirm');
  Route::post('/expenses/insert', [ExpensesController::class, 'insert'])->name('expenses.insert');

  Route::get('/transfers', [TransfersController::class, 'index'])->name('transfers.index');
  Route::get('/transfers/add', [TransfersController::class, 'add'])->name('transfers.add');
  Route::post('/transfers/insert', [TransfersController::class, 'insert'])->name('transfers.insert');
  Route::get('/transfers/detail/{id}', [TransfersController::class, 'detail'])->name('transfers.detail');
  Route::get('/transfers/dispute/{id}', [TransfersController::class, 'dispute'])->name('transfers.dispute');
  Route::get('/transfers/confirm/{id}', [TransfersController::class, 'confirm'])->name('transfers.confirm');

  Route::get('/profile', [UserController::class, 'profile'])->name('user.profile');

  Route::post('/profile/update/profile', [UserController::class, 'update'])->name('user.update.profile');
  Route::post('/profile/update/password', [UserController::class, 'changePassword'])->name('user.update.password');
});


