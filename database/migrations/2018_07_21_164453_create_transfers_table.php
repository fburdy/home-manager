<?php

use App\Models\Transfer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('transfers', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('amount', 11, 6);
      $table->string('description', 200)->nullable();
      $table->unsignedBigInteger('creator_id');
      $table->unsignedBigInteger('from_user_id');
      $table->unsignedBigInteger('to_user_id');
      $table->unsignedSmallInteger('state')->default(Transfer::STATUS_WAITING);
      $table->foreign('creator_id')->references('id')->on('users');
      $table->foreign('from_user_id')->references('id')->on('users');
      $table->foreign('to_user_id')->references('id')->on('users');
      $table->date('value_date')->useCurrent();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('transfers', function (Blueprint $table) {
      $table->dropForeign('transfers_from_user_id_foreign');
      $table->dropForeign('transfers_to_user_id_foreign');
      $table->dropForeign('transfers_creator_id_foreign');
    });

    Schema::dropIfExists('transfers');
  }
}
