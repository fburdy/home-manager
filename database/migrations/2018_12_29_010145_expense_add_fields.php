<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpenseAddFields extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('expenses', function (Blueprint $table) {
      $table->unsignedBigInteger('organization_id')->nullable();
      $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('set null');;
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('expenses', function (Blueprint $table) {
      $table->dropForeign('expenses_organization_id_foreign');
      $table->dropColumn('organization_id');
    });
  }
}
