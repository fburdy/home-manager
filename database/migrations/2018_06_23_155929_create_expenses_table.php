<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('expense_types', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 60);
      $table->unsignedInteger('parent_id')->nullable();
      $table->foreign('parent_id')->references('id')->on('expense_types')->onDelete('set null');
      $table->timestamps();
    });

    Schema::create('expenses', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('amount', 8, 2);
      $table->unsignedBigInteger('creator_id')->nullable();
      $table->unsignedInteger('expense_type')->nullable();
      $table->string('description', 200)->nullable();
      $table->date('value_date')->useCurrent();
      $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
      $table->foreign('expense_type')->references('id')->on('expense_types')->onDelete('set null');
      $table->timestamps();
    });

    Schema::create('expenses_participants', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('amount', 9, 4);
      $table->unsignedSmallInteger('state')->default(0);
      $table->unsignedBigInteger('user_id');
      $table->unsignedBigInteger('expense_id');
      $table->foreign('user_id')->references('id')->on('users');
      $table->foreign('expense_id')->references('id')->on('expenses')->onDelete('cascade');
      $table->timestamps();
      $table->unique(['user_id', 'expense_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('expense_types', function (Blueprint $table) {
      $table->dropForeign('expense_types_parent_id_foreign');
    });

    Schema::table('expenses', function (Blueprint $table) {
      $table->dropForeign('expenses_creator_id_foreign');
      $table->dropForeign('expenses_expense_type_foreign');
    });

    Schema::table('expenses_participants', function (Blueprint $table) {
      $table->dropForeign('expenses_participants_user_id_foreign');
      $table->dropForeign('expenses_participants_expense_id_foreign');
    });

    Schema::dropIfExists('expenses_participants');
    Schema::dropIfExists('expenses');
    Schema::dropIfExists('expense_types');
  }
}
