<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('organizations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name', 60);
      $table->string('siret', 20)->nullable()->unique();
      $table->unsignedInteger('expense_type_default_id')->nullable();
      $table->foreign('expense_type_default_id')->references('id')->on('expense_types')->onDelete('set null');;
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('organizations', function (Blueprint $table) {
      $table->dropForeign('organizations_expense_type_default_id_foreign');
    });

    Schema::dropIfExists('organizations');
  }
}
