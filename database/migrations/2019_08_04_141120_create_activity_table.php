<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('activity', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name', 60);
      $table->string('description', 200)->nullable();
      $table->unsignedBigInteger('owner_id');
      $table->foreign('owner_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('activity_users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('user_id');
      $table->unsignedBigInteger('activity_id');
      $table->date('validation_date')->nullable();
      $table->string('invitation_token', 60)->unique();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->foreign('activity_id')->references('id')->on('activity')->onDelete('cascade');
      $table->unsignedTinyInteger('role')->default(User::ROLE_PARTICIPANT);
      $table->timestamps();
      $table->unique(['user_id', 'activity_id']);
    });

    Schema::table('expenses', function (Blueprint $table) {
      $table->unsignedBigInteger('activity_id')->after('expense_type');
      $table->foreign('activity_id')->references('id')->on('activity');
    });

    Schema::table('transfers', function (Blueprint $table) {
      $table->unsignedBigInteger('activity_id')->nullable()->after('to_user_id');
      $table->foreign('activity_id')->references('id')->on('activity');
    });

    Schema::table('users', function (Blueprint $table) {
      $table->unsignedBigInteger('current_activity_id')->nullable()->after('remember_token');
      $table->foreign('current_activity_id')->references('id')->on('activity')->onDelete('set null');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->dropForeign('users_current_activity_id_foreign');
      $table->dropColumn('current_activity_id');
    });


    Schema::table('expenses', function (Blueprint $table) {
      $table->dropForeign('expenses_activity_id_foreign');
      $table->dropColumn('activity_id');
    });

    Schema::table('transfers', function (Blueprint $table) {
      $table->dropForeign('transfers_activity_id_foreign');
      $table->dropColumn('activity_id');
    });

    Schema::table('activity_users', function (Blueprint $table) {
      $table->dropForeign('activity_users_user_id_foreign');
      $table->dropForeign('activity_users_activity_id_foreign');
    });

    Schema::table('activity', function (Blueprint $table) {
      $table->dropForeign('activity_owner_id_foreign');
    });

    Schema::dropIfExists('activity_users');

    Schema::dropIfExists('activity');
  }
}
