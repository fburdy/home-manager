<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('payments', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->date('payment_date');
      $table->decimal('amount', 9, 4);
      $table->unsignedBigInteger('payer_id');
      $table->unsignedBigInteger('expense_id');
      $table->foreign('expense_id')->references('id')->on('expenses');
      $table->foreign('payer_id')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('payments', function (Blueprint $table) {
      $table->dropForeign('payments_payer_id_foreign');
      $table->dropForeign('payments_expense_id_foreign');
    });

    Schema::dropIfExists('payments');
  }
}
