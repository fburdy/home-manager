<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Transfer;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransfersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   * @throws Exception
   */
  public function run()
  {
    DB::table('transfers')->delete();
    /** @var User $user */
    $user = User::where('email', 'francois@burdy.fr')->first();
    $activity1 = Activity::where('name', 'Activity exemple 1')->first();
    $outsidersIds = User::where('id', '<>', $user->id)->get()->pluck('id')->toArray();
    $descriptions = [
        'fr' => [
            'Remboursement week-end',
            'Remboursement apéro bar',
            'Remboursement restaurant',
            'Remboursement fast-food',
            'Remboursement vacances',
            'Solde week-end',
            'Solde apéro bar',
            'Solde restaurant',
            'Solde fast-food',
            'Solde vacances',
            'Arrangement week-end',
            'Arrangement apéro bar',
            'Arrangement restaurant',
            'Arrangement fast-food',
            'Arrangement vacances',
            'Avance week-end',
            'Avance apéro bar',
            'Avance restaurant',
            'Avance fast-food',
            'Avance vacances',
        ],
        'en' => [
            'Week-end repayment',
            'Week-end balance',
            'Advance for week-end trip',
            'Advance for holiday travel',
            'Advance for ths holidays',
            'Bar drinks repayment',
            'Restaurant repayment',
            'Restaurant balance',
            'Holiday trip refund',
            'cash loan',
            'cash advance',
            'gas bill payment',
            'account balance',
        ]

    ];

    for ($i = 0, $iMax = 20; $i < $iMax; $i++) {
      $otherId = $outsidersIds[array_rand($outsidersIds)];
      $locale = app()->getLocale();
      if (random_int(0, 3) === 1) {
        DB::table('transfers')->insert([
            [
                'amount' => random_int(4, 15000) / 100,
                'description' => $descriptions[$locale][array_rand($descriptions[$locale])],
                'from_user_id' => $otherId,
                'to_user_id' => $user->id,
                'creator_id' => random_int(0, 1) ? $user->id : $otherId,
                'state' => random_int(0, 4) === 1 ? (Transfer::STATUS_DISPUTED) : Transfer::STATUS_CONFIRMED,
                'value_date' => date('Y-m-d', strtotime('-' . random_int(0, 500) . ' days')),
                'activity_id' => $activity1->id,
            ]
        ]);
      } else {
        DB::table('transfers')->insert([
            [
                'amount' => random_int(4, 15000) / 100,
                'description' => $descriptions[$locale][array_rand($descriptions[$locale])],
                'from_user_id' => $otherId,
                'to_user_id' => $user->id,
                'creator_id' => random_int(0, 1) ? $user->id : $otherId,
                'state' => random_int(0, 4) === 1 ? (Transfer::STATUS_WAITING) : Transfer::STATUS_CONFIRMED,
                'value_date' => date('Y-m-d', strtotime('-' . random_int(0, 500) . ' days')),
                'activity_id' => $activity1->id,
            ]
        ]);
      }
    }

    for ($i = 0, $iMax = random_int(80, 120); $i < $iMax; $i++) {
      $otherId = $outsidersIds[array_rand($outsidersIds)];
      DB::table('transfers')->insert([
          [
              'amount' => random_int(4, 15000) / 100,
              'description' => $descriptions[$locale][array_rand($descriptions[$locale])],
              'from_user_id' => $user->id,
              'to_user_id' => $otherId,
              'creator_id' => random_int(0, 1) ? $user->id : $otherId,
              'state' => random_int(0, 4) === 1 ? (random_int(0, 3) === 1 ? Transfer::STATUS_DISPUTED : Transfer::STATUS_WAITING) : Transfer::STATUS_CONFIRMED,
              'value_date' => date('Y-m-d', strtotime('-' . random_int(0, 500) . ' days')),
              'activity_id' => $activity1->id,
          ]
      ]);
    }

    for ($i = 0; $i < 300; $i++) {
      $fromId = $outsidersIds[array_rand($outsidersIds)];
      do {
        $toId = $outsidersIds[array_rand($outsidersIds)];
      } while ($toId === $fromId);

      DB::table('transfers')->insert([
          [
              'amount' => random_int(4, 15000) / 100,
              'description' => $descriptions[$locale][array_rand($descriptions[$locale])],
              'from_user_id' => $fromId,
              'to_user_id' => $toId,
              'creator_id' => random_int(0, 1) ? $fromId : $toId,
              'state' => random_int(0, 4) === 1 ? (random_int(0, 3) === 1 ? Transfer::STATUS_DISPUTED : Transfer::STATUS_WAITING) : Transfer::STATUS_CONFIRMED,
              'value_date' => date('Y-m-d', strtotime('-' . random_int(0, 500) . ' days')),
              'activity_id' => $activity1->id,
          ]
      ]);
    }
  }
}
