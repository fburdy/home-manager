<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExpenseTypesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('expense_types')->delete();

    DB::table('expense_types')->insert([
        ['id' => 1, 'name' => 'Vie courante'],
        ['id' => 2, 'name' => 'Logement, immobilier'],
        ['id' => 3, 'name' => 'Loisirs, voyages'],
        ['id' => 4, 'name' => 'Transport, véhicule'],
        ['id' => 5, 'name' => 'Services'],
        ['id' => 6, 'name' => 'Santé, prévoyance'],
        ['id' => 7, 'name' => 'Impôts et taxes'],
        ['id' => 8, 'name' => 'Famille'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 1, 'name' => 'Courses, supermarché'],
        ['parent_id' => 1, 'name' => 'Restaurant, snacking'],
        ['parent_id' => 1, 'name' => 'Cadeaux offerts'],
        ['parent_id' => 1, 'name' => 'Shopping, habits'],
        ['parent_id' => 1, 'name' => 'Tabac'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 2, 'name' => 'Loyer'],
        ['parent_id' => 2, 'name' => 'Charges logement'], // sub eau, gaz, elec, chauffage
        ['parent_id' => 2, 'name' => 'Mobilier, équipement'],
        ['parent_id' => 2, 'name' => 'Travaux, artisans, jardin'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 3, 'name' => 'Sorties, soirées'],
        ['parent_id' => 3, 'name' => 'Multimédia, gadgets'],
        ['parent_id' => 3, 'name' => 'Sports, cotis. équipement'],
        ['parent_id' => 3, 'name' => 'Musique, livres, films'],
        ['parent_id' => 3, 'name' => 'Passion, hobby'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 4, 'name' => 'Transports en commun'],
        ['parent_id' => 4, 'name' => 'Achat véhicule, mensualités'],
        ['parent_id' => 4, 'name' => 'Assurance véhicule'],
        ['parent_id' => 4, 'name' => 'Carburant'],
        ['parent_id' => 4, 'name' => 'Entretien, équipements véhicule'],
        ['parent_id' => 4, 'name' => 'Stationnement, garage'],
        ['parent_id' => 4, 'name' => 'Location véhicule'],
        ['parent_id' => 4, 'name' => 'Taxi, VTC'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 5, 'name' => 'Formation, éducation'], // sub-categ ?
        ['parent_id' => 5, 'name' => 'Frais bancaires, intérêts'],
        ['parent_id' => 5, 'name' => 'Conseil financier, juridique'],
        ['parent_id' => 5, 'name' => 'Poste, transporteurs'],
        ['parent_id' => 5, 'name' => 'Coiffeur, esthétique'],
        ['parent_id' => 5, 'name' => 'Internet, TV, VOD'],
        ['parent_id' => 5, 'name' => 'Ménage, aide-à-domicile'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 6, 'name' => 'Cotisations, mutuelle'],
        ['parent_id' => 6, 'name' => 'Médicaments, traitements'],
        ['parent_id' => 6, 'name' => 'Médecin, dentiste, kiné'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 7, 'name' => 'Impôts sur le revenu'],
        ['parent_id' => 7, 'name' => 'Impôts fonciers, patrimoine'],
        ['parent_id' => 7, 'name' => 'Taxe d\'habitation, foncière'],
        ['parent_id' => 7, 'name' => 'Contributions sociales'],
    ]);

    DB::table('expense_types')->insert([
        ['parent_id' => 7, 'name' => 'Activités enfants'],
        ['parent_id' => 7, 'name' => 'Argent de poche'],
        ['parent_id' => 7, 'name' => 'Garde d\'enfant'],
        ['parent_id' => 7, 'name' => 'Jouets, cadeaux'],
        ['parent_id' => 7, 'name' => 'Pension versée'],
        ['parent_id' => 7, 'name' => 'Frais études, scolaire'],
        ['parent_id' => 7, 'name' => 'Pension et dons versés'],
    ]);

  }
}
