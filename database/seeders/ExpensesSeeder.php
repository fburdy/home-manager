<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Expense;
use App\Models\ExpenseParticipant;
use App\Models\ExpenseType;
use App\Models\Payment;
use App\Models\User;
use App\Utils\ViewUtils;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExpensesSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('expenses')->delete();
    $me = User::where('email', 'francois@burdy.fr')->first();
    $allOthers = User::where('id', '!=', $me->id)->get();
    $activity1 = Activity::where('name', 'Activity exemple 1')->first();
    $types = ExpenseType::all();

    for ($i = 0; $i < 30; $i++) {
      $amount = random_int(0.5 * 100, 400 * 100) / 100;
      $expense = Expense::create([
          'amount' => $amount,
          'creator_id' => $me->id,
          'expense_type' => $types->random()->id,
          'activity_id' => $activity1->id,
          'description' => ViewUtils::randomActivityName(),
          'value_date' => date('Y-m-d', strtotime('-' . random_int(0, 500) . ' days')),
      ]);

      $others = $allOthers->random(random_int(0, $allOthers->count()));

      ExpenseParticipant::create([
          'amount' => $expense->amount / ($others->count() + 1),
          'user_id' => $me->id,
          'expense_id' => $expense->id,
          'state' => random_int(0, 4) === 1 ? (random_int(0, 3) === 1 ? ExpenseParticipant::STATUS_DISPUTED : ExpenseParticipant::STATUS_WAITING_VALIDATION) : ExpenseParticipant::STATUS_CONFIRMED
      ]);

      foreach ($others as $other) {
        ExpenseParticipant::create([
            'amount' => $expense->amount / ($others->count() + 1),
            'user_id' => $other->id,
            'expense_id' => $expense->id,
            'state' => random_int(0, 4) === 1 ? (random_int(0, 3) === 1 ? ExpenseParticipant::STATUS_DISPUTED : ExpenseParticipant::STATUS_WAITING_VALIDATION) : ExpenseParticipant::STATUS_CONFIRMED
        ]);
      }

      $payers = $others->add($me)->random(random_int(1, $others->count()));
      foreach ($payers as $payer) {
        Payment::create([
            'amount' => $expense->amount / $payers->count(),
            'payment_date' => $expense->value_date,
            'payer_id' => $payer->id,
            'expense_id' => $expense->id,
        ]);
      }
    }
  }
}
