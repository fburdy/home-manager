<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\User;
use App\Utils\Utils;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   * @throws Exception
   */
  public function run()
  {
    DB::table('activity')->delete();
    /** @var User $me */
    $me = User::where('email', 'francois@burdy.fr')->first();
    $other = User::where('email', 'john@doe.com')->first();
    DB::table('activity')->insert([
        [
            'name' => 'Activity exemple 1',
            'description' => 'Ceci est une activité d\'exemple',
            'owner_id' => $me->id
        ],
        [
            'name' => 'Activity exemple 2',
            'description' => 'Ceci est une autre activité d\'exemple',
            'owner_id' => $other->id
        ],
        [
            'name' => 'Activity exemple 3',
            'description' => 'Ceci est encore activité d\'exemple',
            'owner_id' => $me->id
        ]
    ]);

    $activity1 = Activity::where('name', 'Activity exemple 1')->first();
    $activity2 = Activity::where('name', 'Activity exemple 2')->first();
    foreach (User::all() as $user) {
      DB::table('activity_users')->insert([
          'user_id' => $user->id,
          'activity_id' => $activity1->id,
          'validation_date' => now()->subDay(),
          'invitation_token' => Utils::randomToken(50),
          'role' => $user->id === $me->id ? User::ROLE_ADMIN : User::ROLE_PARTICIPANT,
      ]);
    }

    DB::table('activity_users')->insert([
        'user_id' => $me->id,
        'activity_id' => $activity2->id,
        'validation_date' => now()->subDay(),
        'invitation_token' => Utils::randomToken(50),
        'role' => $user->id === $me->id ? User::ROLE_ADMIN : User::ROLE_PARTICIPANT,
    ]);

    DB::table('users')
        ->where('email', 'francois@burdy.fr')
        ->update(['current_activity_id' => $activity1->id]);
  }
}
