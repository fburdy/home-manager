<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    $this->call(UsersTableSeeder::class);
    $this->call(ActivityTableSeeder::class);
    $this->call(TransfersTableSeeder::class);
    $this->call(OrganizationsTableSeeder::class);
    $this->call(ExpenseTypesTableSeeder::class);
    $this->call(ExpensesSeeder::class);
  }
}
