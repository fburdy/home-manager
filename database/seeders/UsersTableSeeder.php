<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->delete();

    DB::table('users')->insert([
        [
            'name' => 'François Burdy',
            'email' => 'francois@burdy.fr',
            'password' => Hash::make('secret'),
        ],
        [
            'name' => 'John Doe',
            'email' => 'john@doe.com',
            'password' => Hash::make('secret'),
        ],
        [
            'name' => 'Hubert De la Batte',
            'email' => 'hubert.delabatte@oss117.com',
            'password' => Hash::make('secret'),
        ],
        [
            'name' => 'Carlotta',
            'email' => 'carlotta@email.com',
            'password' => Hash::make('secret'),
        ],
        [
            'name' => 'Lucien Bramart',
            'email' => 'lucien.bramart@gmail.com',
            'password' => Hash::make('secret'),
        ],

    ]);
  }
}
