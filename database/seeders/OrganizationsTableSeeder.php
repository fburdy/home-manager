<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrganizationsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('organizations')->delete();

    DB::table('organizations')->insert([
        [
            'name' => 'Carrefour Meylan',
            'siret' => '45132133501120',
        ],
        [
            'name' => "McDonald's",
            'siret' => '52247793400023',
        ],
        [
            'name' => 'Boucherie Sanzot',
            'siret' => '48243416400018',
        ],
        [
            'name' => 'Metro',
            'siret' => '43163745300017',
        ],
        [
            'name' => 'Foncia',
            'siret' => '35305263200018',
        ],
    ]);
  }
}
