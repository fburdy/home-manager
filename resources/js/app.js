require('./bootstrap');

$(document).ready(function($) {
  $(".clickable-row").click(function() {
    window.location = $(this).data("href");
  });

  $("body").on('submit', 'form', function() {
    var spinner = $('<span class="spinner-grow spinner-grow-sm mr-1" role="status" aria-hidden="true"></span>');
    $(this).find('.btn.spin-submit').prepend(spinner)
  });
});

