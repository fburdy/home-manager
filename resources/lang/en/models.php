<?php

return [
    'activity' => 'activity',
    'activities' => 'activities',
    'activity.add' => 'Start an activity',
    'activity.name' => 'Activity name',
    'participate.in.activities' => "{0} You are not participating in any activities at this time|{1} You participate in a single activity|[2,*] You participate in :nb activities",

    'organizations' => 'organizations',

    'expense' => 'expense',
    'expense.already.confirmed' => 'You have already confirmed this expense',
    'expense.confirmed' => 'Expense :name confirmed.',
    'expense.date' => 'expense date',
    'expense.desc.placeholder' => '&laquo; Groceries... &raquo;, &laquo; gas bill... &raquo;...',
    'expense.disputed' => 'Expense :name disputed',
    'expense.not.found' => 'Expense not found',
    'expense.not.involved' => 'You are not involved in this expense',
    'expense.saved' => 'Expense saved',
    'expense.type' => 'expense type',
    'expenses' => 'expenses',
    'expenses.add' => 'Add a new expense',
    'expenses.new' => 'New expense',
    'expenses.to.confirm' => 'expenses to confirm',
    'all.my.expenses' => 'All my expenses',
    'no.expense.yet' => 'No expense at the moment',

    'dashboard' => 'dashboard',
    'debts-receivable' => 'Debts & Receivable',

    'transfer' => 'transfer',
    'transfer.received.from' => 'transfer received from',
    'transfer.sent.to' => 'transfer sent to',
    'transfers' => 'transfers',
    'transfer.add' => 'Add a new transfer',
    'transfer.desc.placeholder' => 'repayment, advance, cash loan...',
    'transfer.new' => 'New transfer',
    'transfer.reason' => 'Reason of the transfer',
    'all.my.transfers' => 'All my transfers',
    'no.transfer.yet' => 'No transfer at the moment',

    'transfers.to.confirm' => 'transfers to confirm',
];
