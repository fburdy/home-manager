<?php

return [
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'email.address' => 'E-mail address',
    'name' => 'Name',
    'name.firstname' => 'Name',

    'password' => 'Password',
    'confirm.password' => 'Confirm password',
    'current.password' => 'Current password',

    'remember.me' => 'Remember me',
    'sign.in' => 'Sign in',
    'sign.in.long' => 'Sign in to your account',
    'no.account.yet?' => 'No account yet?',
    'already.have.account?' => 'Already have an account?',
    'register' => 'Register',
    'register.long' => 'Create your Home Manager account',
    'sign.out' => 'Sign out',
    'register.conditions' => "I accept that I will receive emails to validate my registration.",

    'user.account' => 'User account',
    'user.informations' => 'user informations',
    'user.change.password' => 'Update password',

    'forgot.password' => 'Forgot your password?',
];
