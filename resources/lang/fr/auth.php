<?php

return [
    'failed' => 'Les identifiants ne sont pas reconnus.',
    'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer dans :seconds secondes.',

    'email.address' => 'Adresse email',
    'name' => 'Nom',
    'name.firstname' => 'Nom et Prénom',

    'password' => 'Mot de passe',
    'confirm.password' => 'Confirmation mot de passe',
    'current.password' => 'Mot de passe actuel',

    'remember.me' => 'Se souvenir de moi',
    'sign.in' => 'Se connecter',
    'sign.in.long' => 'Connectez-vous à votre compte',
    'no.account.yet?' => 'Vous n\'avez pas encore de compte ?',
    'already.have.account?' => 'Vous avez déjà un compte ?',
    'register' => 'S\'inscrire',
    'register.long' => 'Créez votre compte Home Manager',
    'sign.out' => 'Se déconnecter',
    'register.conditions' => "J'accepte de recevoir des emails pour valider mon inscription.",

    'user.account' => 'Mon compte',
    'user.informations' => 'Mes informations utilisateur',
    'user.change.password' => 'Modifier mon mot de passe',

    'forgot.password' => 'Mot de passe oublié ?',
];
