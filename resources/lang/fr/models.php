<?php

return [
    'activity' => 'activité',
    'activities' => 'activités',
    'activity.add' => 'Commencer une activité',
    'activity.name' => 'Nom de l\'activité',
    'participate.in.activities' => "{0} Vous ne participez à aucune activité pour le moment|{1} Vous participez à une seule activité|[2,*] Vous participez à :nb activités",

    'organizations' => 'organisations',

    'expense' => 'dépense',
    'expense.already.confirmed' => 'Vous avez déjà confirmé cette dépense',
    'expense.confirmed' => 'Dépense :name confirmée.',
    'expense.date' => 'Date de la dépense',
    'expense.desc.placeholder' => '&laquo; Courses... &raquo;, &laquo; facture... &raquo;...',
    'expense.disputed' => 'Dépense :name contestée.',
    'expense.not.found' => 'Dépense introuvable',
    'expense.not.involved' => 'Vous n\'êtes pas concerné par cette dépense',
    'expense.saved' => 'Expense saved',
    'expense.type' => 'Type de dépense',
    'expenses' => 'dépenses',
    'expenses.add' => 'Ajouter une dépense',
    'expenses.new' => 'Nouvelle dépense',
    'expenses.to.confirm' => 'dépenses à confirmer',
    'all.my.expenses' => 'Toutes mes dépenses',
    'no.expense.yet' => 'Aucune dépense pour le moment',

    'dashboard' => 'tableau de bord',
    'debts-receivable' => 'Dettes & Créances',

    'transfer' => 'versement',
    'transfer.received.from' => 'versement reçu de',
    'transfer.sent.to' => 'versement envoyé à',
    'transfers' => 'versements',
    'transfer.add' => 'Ajouter un versement',
    'transfer.desc.placeholder' => 'remboursement, avance, cash...',
    'transfer.new' => 'Nouveau versement',
    'transfer.reason' => 'Motif du versement',
    'all.my.transfers' => 'Tous mes versements',
    'no.transfer.yet' => 'Aucun versement pour le moment',

    'transfers.to.confirm' => 'versements à confirmer',
];
