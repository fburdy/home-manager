<?php

return [
    'password' => 'Le mots de passe doit contenir au moins 6 caractères, et la confirmation doit correspondre.',
    'reset' => 'Votre mot de passe a été réinitialisé !',
    'sent' => 'Nous vous avons envoyé un email de réinitialisation de mot de passe.',
    'token' => 'Ce code de réinitialisation de mot de passe est invalide.',
    'user' => "Nous ne trouvons pas d'utilisateur pour cette adresse email.",
];
