@extends('layouts.app', ['activeNav' => 'dashboard'])

@section('title', ucfirst(__('models.transfers')))

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-8">
        @include('dashboard.card-debts-receivable')
      </div>
      <div class="col-12 col-lg-4">
        @includeWhen($waitingUserActivities->count() > 0, 'dashboard.waiting-invitations')
        @if($currentActivity === null)
          @include('activity.add-activity-form')
        @else
          @include('dashboard.expenses-to-confirm')
          @include('dashboard.transfers-to-confirm')
        @endif
      </div>
    </div>
  </div>
@endsection
