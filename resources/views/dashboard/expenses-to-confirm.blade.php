<h3 class="px-1 d-none d-sm-block mt-2">{{ $waitingExpenseParticipants->count() }} {{ __('models.expenses.to.confirm') }}</h3>
<h1 class="fw-semi-bold px-3 d-block d-sm-none mt-3 mb-3">{{ $waitingExpenseParticipants->count() }} {{ __('models.expenses.to.confirm') }}</h1>

<div class="card mb-4 shadow">
  @if($waitingExpenseParticipants->count() > 0)
    <div class="card">
      <ul class="list-group list-group-flush">
        @foreach($waitingExpenseParticipants as $expenseParticipant)
          @php
            /** @var \App\Models\ExpenseParticipant $expenseParticipant */
            $expense = $expenseParticipant->expense;
          @endphp
          <li class="list-group-item fw-semi-bold py-3">
            <div class="row">
              <div class="col-9">
                <i class="fas fa-shopping-cart"></i>
                <strong>{{ $expense->description }}</strong><br/>
                <strong class="smaller text-secondary text-nowrap">
                  {{ \App\Utils\Utils::relativeDateFrench($expense->value_date) }}
                </strong>
              </div>
              <div class="col-3 text-end">
                <strong class="fs-5 text-nowrap">
                  {{ \App\Utils\Utils::asPrice($expenseParticipant->amount) }}
                </strong>
              </div>
              <div class="col-12 text-end mt-1">
                <a href="{{ route('expenses.dispute', ['id' => $expense->id]) }}"
                   class="btn btn-danger btn-sm fw-bold text-white text-nowrap ms-1">{{ __('misc.dispute') }}</a>
                <a href="{{ route('expenses.detail', ['id' => $expense->id]) }}"
                   class="btn btn-secondary btn-sm fw-bold text-nowrap ms-1">{{ __('misc.details') }}</a>
                <a href="{{ route('expenses.confirm', ['id' => $expense->id]) }}"
                   class="btn btn-primary btn-sm fw-bold text-white text-nowrap ms-1">{{ __('misc.confirm') }}</a>
              </div>
            </div>
          </li>
        @endforeach
      </ul>
    </div>
  @elseif($user->expenseParticipants->isEmpty())
    <p class="font-italic fw-bold text-secondary bigger pt-3 px-3">
      Aucune dépense pour le moment
    </p>
  @else
    <p class="font-italic fw-bold text-secondary bigger pt-3 px-3">
      Toutes les dépenses sont traitées 🤞
    </p>

  @endif
</div>
