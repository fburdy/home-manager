<h3 class="px-1 d-none d-sm-block mt-2">{{ ucfirst(__('models.dashboard')) }}</h3>
<h1 class="fw-semi-bold px-3 d-block d-sm-none mt-3 mb-3">{{ ucfirst(__('models.dashboard')) }}</h1>

<div class="card max-h-500 mb-3 shadow">
  <div class="card-body">
    @if (session('status'))
      <div class="alert alert-success">{{ session('status') }}</div>
    @endif
    @php
      /** @var \App\Models\User $user */
      $payable = $debtsBalance['totalPayable'];
      $receivable = $debtsBalance['totalReceivable'];
      $showProgress = $payable + $receivable > 0.0;
      $total = $debtsBalance['totalBalance'];
      $ratio = $showProgress ? $receivable / ($payable + $receivable) : null;
    @endphp

      <div class="fs-5 mb-4 mt-2">
        {{ ucfirst(__('misc.you.are.owed')) }}
        <strong class="text-nowrap fw-bold text-success">
          {{ \App\Utils\Utils::asPrice($receivable, '€', 0) }}
        </strong>
        {{ __('misc.and') }} {{ __('misc.you.owe') }}
        <strong class="text-nowrap fw-bold text-danger">
          {{ \App\Utils\Utils::asPrice($payable, '€', 0) }}
        </strong>
        {{ __('misc.for.a.net.balance.of') }}
        <strong class="text-nowrap fw-bold {{ $total > 0 ? 'text-success' : ''}}">
          {{ \App\Utils\Utils::asPrice($total, '€', 0) }}
        </strong>.
        @if($showProgress)
          <div class="bg-danger rounded text-nowrap mt-2" style="height: 8px">
            <div class="bg-success rounded-left text-center border-right"
                 style="height: 8px; width: {{ $ratio * 100 }}%">
              <div style="margin: -14px -12px -8px 0;" class="d-inline-block float-end">
                <i class="material-icons">arrow_drop_down</i>
              </div>
            </div>
          </div>
        @endif
      </div>

    @foreach($debtsBalance['members'] as $member)
      @if($member['balanceTotal'] >= 0.01)
        <div class="alert alert-success d-inline-block mr-2 mb-2" role="alert">
          <strong>{{ $member['user']->name }}</strong> {{ __('misc.owes.you') }}
          <strong class="text-nowrap">
            {{ \App\Utils\Utils::asPrice($member['balanceTotal']) }}
          </strong>
        </div>
      @elseif($member['balanceTotal'] <= -0.01)
        <div class="alert alert-danger d-inline-block mr-2 mb-2" role="alert">
          {{ ucfirst(__('misc.you.owe')) }}
          <strong class="text-danger text-nowrap">
            {{ \App\Utils\Utils::asPrice($member['balanceTotal'] * -1) }}
          </strong>
          {{ __('misc.to') }} <strong>{{ $member['user']->name }}</strong>
        </div>
      @else
        <div class="alert alert-secondary d-inline-block mr-2 mb-2" role="alert">

          {!! __('misc.account.is.balanced', ['name' => $member['user']->name]) !!}
        </div>
      @endif
    @endforeach
  </div>
</div>
