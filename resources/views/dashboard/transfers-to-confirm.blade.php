<h3 class="px-1 d-none d-sm-block mt-2">{{ $waitingTransfers->count() }} {{ __('models.transfers.to.confirm') }}</h3>
<h1 class="fw-semi-bold px-3 d-block d-sm-none mt-3 mb-3">{{ $waitingTransfers->count() }} {{ __('models.transfers.to.confirm') }}</h1>

<div class="card mb-3 shadow">
  @if($waitingTransfers->count() > 0)
    <div class="card">
      <ul class="list-group list-group-flush">
        @foreach ($waitingTransfers as $transfer)
          <li class="list-group-item fw-semi-bold py-3">
            <div class="row">
              <div class="col-9">
                <i class="fas fa-exchange-alt"></i>
                <strong>{{ $transfer->description }}</strong><br/>
                <strong class="smaller text-secondary text-nowrap">
                  {{ \App\Utils\Utils::relativeDateFrench($transfer->value_date) }}
                </strong>
              </div>
              <div class="col-3 text-end">
                <strong class="fs-5 text-nowrap">
                  {{ \App\Utils\Utils::asPrice($transfer->amount) }}
                </strong>
              </div>
              <div class="col-12 text-end mt-1">
                <a href="{{ route('transfers.dispute', ['id' => $transfer->id]) }}"
                   class="btn btn-danger btn-sm fw-bold text-white text-nowrap ms-1">{{ __('misc.dispute') }}</a>
                <a href="{{ route('transfers.detail', ['id' => $transfer->id]) }}"
                   class="btn btn-secondary btn-sm fw-bold text-nowrap ms-1">{{ __('misc.details') }}</a>
                <a href="{{ route('transfers.confirm', ['id' => $transfer->id]) }}"
                   class="btn btn-primary btn-sm fw-bold text-white text-nowrap ms-1">{{ __('misc.confirm') }}</a>
              </div>
            </div>
          </li>
        @endforeach
      </ul>
    </div>
  @elseif($user->receivedTransfers->isEmpty() && $user->sentTransfers->isEmpty())
    <p class="font-italic fw-bold text-secondary bigger pt-3 px-3">
      Aucun versement pour le moment
    </p>
  @else
    <p class="font-italic fw-bold text-secondary bigger pt-3 px-3">
      Tous les versements sont traités 🤞
    </p>
  @endif
</div>
