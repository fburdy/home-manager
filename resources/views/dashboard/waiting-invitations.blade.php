<h3 class="px-1 d-none d-sm-block mt-2">{{ $waitingUserActivities->count() }} invitations en attente</h3>
<h1 class="fw-semi-bold px-3 d-block d-sm-none mt-3 mb-3">{{ $waitingUserActivities->count() }} invitations en
  attente</h1>

<div class="card mb-4 shadow">

  <div class="card">
    <ul class="list-group list-group-flush">
      @foreach($waitingUserActivities as $waitingUserActivity)
        @php
          /** @var \App\Models\ActivityUser $waitingUserActivity */
          $waitingActivity = $waitingUserActivity->activity;
        @endphp
        <li class="list-group-item fw-semi-bold py-3">
          <i class="fas fa-briefcase"></i>
          <strong>{{ $waitingActivity->name }}</strong><br/>
          <em>{{ $waitingActivity->description }}</em>
          <div class="text-end mt-1">
            <a href="{{ route('activity.detail', ['id' => $waitingActivity->id]) }}"
               class="btn btn-secondary btn-sm fw-bold text-nowrap ms-1">Détails</a>
            <a href="{{ route('activity.declineInvitation', ['token' => $waitingUserActivity->invitation_token]) }}"
               class="btn btn-warning btn-sm fw-bold text-nowrap ms-1">Décliner</a>
            <a href="{{ route('activity.confirmInvitation', ['token' => $waitingUserActivity->invitation_token]) }}"
               class="btn btn-primary btn-sm fw-bold text-white text-nowrap ms-1">Accepter l'invitation</a>
          </div>
        </li>
      @endforeach
    </ul>
  </div>

</div>
