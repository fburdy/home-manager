@extends('layouts.app')

@section('title', __('auth.sign.in'))

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 col-lg-7 col-xl-6">

        <h1 class="fw-semi-bold fs-2  mt-lg-4">
          {{ __('auth.sign.in.long') }}
        </h1>
        <div class="card shadow mt-3 mt-lg-4">
          <div class="card-body">

            <div class="col-lg-10 offset-lg-1 col-xxl-8 offset-xxl-2">
              <form method="POST" action="{{ route('login') }}" class="">
                @csrf
                <div class="mb-2 mb-lg-3 mt-lg-3">
                  <label for="email" class="col-form-label ">{{ __('auth.email.address') }}</label>
                  <input id="email" type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                         name="email" value="{{ old('email') }}" required autofocus>

                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="mb-3 mb-lg-4">
                  <label for="password" class="col-form-label">
                    {{ __('auth.password') }}
                  </label>
                  <input id="password" type="password" name="password"
                         class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="mb-3 mb-lg-4 form-check">
                  <input class="form-check-input" type="checkbox" name="remember"
                         id="rememberMeCheckbox" {{ old('remember') ? 'checked' : '' }}>
                  <label class="form-check-label" for="rememberMeCheckbox">
                    {{ __('auth.remember.me') }}
                  </label>
                </div>

                <div class="d-grid mb-2">
                  <button type="submit" class="btn btn-primary fw-semi-bold btn-lg spin-submit">
                    {{ __('auth.sign.in') }}
                  </button>
                  <a class="btn btn-link mt-3" href="{{ route('password.request') }}">
                    {{ __('auth.forgot.password') }}
                  </a>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="card mt-3 shadow">
          <div class="card-body fw-semi-bold text-center">
            {{ __('auth.no.account.yet?') }}
            <a href="{{ route('register') }}" class="pl-1">{{ __('auth.register') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
