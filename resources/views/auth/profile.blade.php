@extends('layouts.app', ['activeNav' => 'account'])

@section('title', __('auth.user.account'))

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 col-xl-6">
        <div class="card mb-4 shadow">
          <div class="card-header fw-bold">{{ ucfirst(__('auth.user.informations')) }}</div>
          <div class="card-body">
            <form method="POST" action="{{ route('user.update.profile') }}">
              @csrf
              <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-end">
                  {{ __('auth.name')  }}
                </label>
                <div class="col-md-7">
                  <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                         name="name" value="{{ $user->name }}" required>
                  @if ($errors->has('name'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group row mt-2">
                <label for="email" class="col-md-4 col-form-label text-md-end">
                  {{ __('auth.email.address') }}
                </label>
                <div class="col-md-7">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                         name="email" value="{{ $user->email }}" required>

                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group row mb-0 mt-3">
                <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary fw-bold spin-submit">
                    {{ __('misc.save') }}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-8 col-xl-6">
        <div class="card mb-4 shadow">
          <div class="card-header fw-bold">{{ __('auth.user.change.password') }}</div>
          <div class="card-body">
            <form method="POST" action="{{ route('user.update.password') }}">
              @csrf
              <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-end">
                  {{ __('auth.current.password') }}
                </label>
                <div class="col-md-7">
                  <input id="current-password" type="password" name="current-password"
                         class="form-control{{ $errors->has('current-password') ? ' is-invalid' : '' }}" required>
                  @if ($errors->has('current-password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('current-password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <hr/>
              <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-end">
                  {{ __('auth.password') }}
                </label>
                <div class="col-md-7">
                  <input id="password" type="password" name="password"
                         class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row mt-2">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-end">
                  {{ __('auth.confirm.password') }}
                </label>
                <div class="col-md-7">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                         required>
                </div>
              </div>

              <div class="form-group row mb-0 mt-3">
                <div class="col-md-7 offset-md-4">
                  <button type="submit" class="btn btn-primary fw-bold spin-submit">
                    {{ __('misc.save') }}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
