@extends('layouts.app')

@section('title', __('auth.register'))

@section('content')
  <div class="container">
    <div class="row justify-content-center">

      <div class="col-md-8 col-lg-7 col-xl-6">
        <h1 class="fw-semi-bold fs-2 mt-1 mt-lg-4">
          {{ __('auth.register.long') }}
        </h1>
        <div class="card shadow mt-4">
          <div class="card-body row">
            <div class="col-lg-10 offset-lg-1 col-xxl-8 offset-xxl-2">
              <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="mb-3 mt-lg-4">
                  <label for="name" class="col-form-label fw-semi-bold">
                    {{ __('auth.name.firstname')  }}
                  </label>
                  <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                         name="name" value="{{ old('name') }}" required autofocus>
                  @if ($errors->has('name'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="mb-3">
                  <label for="email" class="col-form-label fw-semi-bold">
                    {{ __('auth.email.address') }}
                  </label>
                  <input id="email" type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                         name="email" value="{{ old('email') }}" required>
                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="mb-3">
                  <label for="password" class="col-form-label fw-semi-bold">
                    {{ __('auth.password') }}
                  </label>
                  <input id="password" type="password" name="password"
                         class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="mb-4">
                  <label for="password-confirm" class="col-form-label fw-semi-bold">
                    {{ __('auth.confirm.password') }}
                  </label>
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                         required>
                </div>
                <div class="mb-4 form-check">
                  <input class="form-check-input" type="checkbox" name="accept-conditions"
                         id="conditionsCheckbox" autocomplete="off" required>
                  <label class="form-check-label" for="conditionsCheckbox">
                    {{ __('auth.register.conditions') }}
                  </label>
                </div>

                <div class="d-grid mb-3 mb-lg-5">
                  <button type="submit" class="btn btn-primary fw-semi-bold btn-lg spin-submit">
                    {{ __('auth.register') }}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="card mt-3 shadow">
          <div class="card-body fw-semi-bold text-center">
            {{ __('auth.already.have.account?') }}
            <a href="{{ route('login') }}" class="pl-1">{{ __('auth.sign.in') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
