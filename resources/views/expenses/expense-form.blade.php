<h3 class="px-1 d-none d-sm-block mt-2">{{ ucfirst(__('models.expenses.add')) }}</h3>
<h1 class="fw-semi-bold px-3 d-block d-sm-none mt-3 mb-3">{{ ucfirst(__('models.expenses.add')) }}</h1>

<div class="card shadow-sm">
  <div class="card-body px-3">
    @if ($errors->any())
      <div class="alert alert-danger pb-1">
        @foreach ($errors->all() as $error)
          <p>{{ $error }}</p>
        @endforeach
      </div>
    @endif

    @if($validatedActivities->count() > 0)
      <form method="post" action="{{ route('expenses.insert') }}">
        @csrf
        <label for="description">Description</label>
        <div class="input-group mb-3">
          <input type="text" id="description" value="{{ old('description') }}" required
                 placeholder="{!! __('models.expense.desc.placeholder') !!}" name="description"
                 class="form-control form-control-lg {{ $errors->has('description') ? 'is-invalid' : '' }}">

          @if ($errors->has('description'))
            <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
          @endif
        </div>

        <label for="amount">{{ ucfirst(__('misc.total.amount')) }}</label>
        <div class="input-group mb-3">
          <input type="number" step="0.01" id="amount" placeholder="{{ ucfirst(__('misc.amount.euros')) }}" name="amount"
                 value="{{ old('amount') }}" required
                 class="form-control form-control-lg {{ $errors->has('amount') ? 'is-invalid' : '' }}">
          <span class="input-group-text">€</span>
          @if ($errors->has('amount'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('amount') }}</strong>
            </span>
          @endif
        </div>

        <label for="expense_type">{{ ucfirst(__('models.expense.type')) }}</label>
        <select id="expense_type" name="expense_type" required
                class="mb-3 form-select form-select-lg {{ $errors->has('expense_type') ? 'is-invalid' : '' }}">
          <option selected disabled value="">{{ ucfirst(__('misc.choose')) }}...</option>
          @foreach($expenseTypes as $expenseType)
            <option value="{{ $expenseType->id }}">{{ $expenseType->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('expense_type'))
          <span class="invalid-feedback"><strong>{{ $errors->first('expense_type') }}</strong></span>
        @endif

        <label for="value_date">{{ ucfirst(__('models.expense.date')) }}</label>
        <div class="input-group mb-3">
          <input type="date" id="value_date" placeholder="Valeur en euros" name="value_date"
                 value="{{ old('value_date') ?? now()->format('Y-m-d') }}" required
                 class="form-control form-control-lg {{ $errors->has('value_date') ? 'is-invalid' : '' }}">
          <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
          @if ($errors->has('value_date'))
            <span class="invalid-feedback"><strong>{{ $errors->first('value_date') }}</strong></span>
          @endif
        </div>

        <label for="activity_id">{{ ucfirst(__('models.activity')) }}</label>
        <select id="activity_id" name="activity_id" required readonly=""
                class="mb-3 form-select form-select-lg {{ $errors->has('activity_id') ? 'is-invalid' : '' }}">
          @foreach($validatedActivities as $activity)
            <option
                value="{{ $activity->id }}" {{ $activity->id === $currentActivity->id ? 'selected' : '' }}>{{ $activity->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('activity_id'))
          <span class="invalid-feedback"><strong>{{ $errors->first('activity_id ') }}</strong></span>
        @endif

        <label class="form-label">Participants</label>
        @forelse($currentActivity->activityUsers()->get() as $key => $activityUser)
          <input type="hidden" name="participants[{{ $key }}][user_id]" value="{{ $activityUser->user_id }}">
          <div class="input-group mb-1">
            <div class="input-group-prepend">
              <div class="input-group-text" style="width:120px">
                <label class="form-check-label text-ellipsis">
                  <input type="checkbox"
                         name="participants[{{ $key }}][active]"
                         aria-label="Checkbox for following text input" checked>
                  <span class="pl-1 smaller">{{ $activityUser->user->name }}</span>
                </label>
              </div>
            </div>
            <input type="number" step="0.01" name="participants[{{ $key }}][share_amount]"
                   class="form-control input-share" placeholder="{{ ucfirst(__('misc.spent.short')) }}">
            <input type="number" step="0.01" name="participants[{{ $key }}][paid_amount]"
                   class="form-control input-paid" placeholder="{{ ucfirst(__('misc.paid')) }}">
          </div>
        @empty
          <div class="alert alert-danger" role="alert">
            Aucun participant à cette activité...
          </div>
        @endforelse

        <div class="row mb-1 mt-3">
          <div class="col-6">
            @if($cancelBtn ?? false)
              <a class="btn btn-secondary fw-bold" href="{{ route('expenses.index') }}">
                <i class="fas fa-arrow-left me-1"></i> {{ ucfirst(__('misc.cancel')) }}
              </a>
            @endif
          </div>
          <div class="col-6 text-end">
            <button type="submit" class="btn btn-primary fw-bold spin-submit">
              <i class="fas fa-check me-1"></i> {{ ucfirst(__('misc.save')) }}
            </button>
          </div>
        </div>
      </form>
    @else
      <div class="alert alert-danger" role="alert">
        Vous n'avez aucune activité, impossible d'ajouter une dépense
      </div>
    @endif
  </div>
</div>
