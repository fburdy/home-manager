@extends('layouts.app', ['activeNav' => 'expenses'])

@section('title', ucfirst(__('models.expenses')))

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="d-none d-lg-block col-sm-12 col-lg-4 mb-3 order-2 order-lg-1 p-0 px-lg-3">
        @include('expenses.expense-form')
      </div>
      <div class="col-sm-12 col-lg-8 mb-3 order-1 order-lg-2 p-0 px-lg-3">
        @include('expenses.expenses-list-card')
      </div>
    </div>
  </div>
@endsection
