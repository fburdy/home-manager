<div class="row">
  <div class="col-6">
    <h3 class="px-1 d-none d-sm-block mt-2">{{ ucfirst(__('models.expenses')) }}</h3>
    <h1 class="fw-semi-bold px-3 d-block d-sm-none mt-1 mb-3">{{ ucfirst(__('models.expenses')) }}</h1>
  </div>
  <div class="col-6 d-lg-none text-end">
    <a class="btn btn-primary fw-bold me-1" href="{{ route('expenses.add') }}">
      <i class="fas fa-plus me-1"></i> {{ ucfirst(__('models.expenses.new')) }}
    </a>
  </div>
</div>

<div class="card shadow-sm">
  <div class="table-responsive px-2">
    <table class="table table-hover">
      <thead class="thead-light">
      <tr>
        <th scope="col" class="text-center d-none d-xl-table-cell">#</th>
        <th scope="col" class="text-center">Date</th>
        <th scope="col">Description</th>
        <th scope="col" class="text-center">{{ __('misc.status') }}</th>
        <th scope="col" class="text-end pe-lg-3 pe-2">{{ ucfirst(__('misc.amount')) }}</th>
      </tr>
      </thead>
      <tbody>
      @forelse($expenses as $expense)
        <tr class='clickable-row' data-href="{{ route('expenses.detail', ['id' => $expense->id]) }}">
          <td class="text-center small d-none d-xl-table-cell">
            <em>{{ $expense->id }}</em>
          </td>
          <td class="smaller text-center">
            <span class="d-none d-xl-block">{{ \App\Utils\Utils::formatDate($expense->value_date) }}</span>
            <span class="d-xl-none">{{ \App\Utils\Utils::formatDate($expense->value_date, true) }}</span>
          </td>
          <td class="smaller">
            {{ $expense->description }}
          </td>

          <td class="bigger text-center">
            {!! \App\Utils\ViewUtils::expenseStatusBadge($expense) !!}
          </td>
          <td class="fw-bold text-end fw-bold text-nowrap pe-lg-3 pe-2">
            {{ \App\Utils\Utils::asPrice($expense->amount) }}
          </td>
        </tr>
      @empty
        <tr>
          <th scope="row" colspan="6" class="pt-4 text-center">
            {{ _('models.no.expense.yet') }}...
          </th>
        </tr>
      @endforelse
      </tbody>
    </table>
  </div>
  @if($expenses->isNotEmpty())
    <div class="card-footer pb-0">
      <div class="d-flex ms-auto fw-bold">{{ $expenses->onEachSide(1)->links() }}</div>
    </div>
  @endif
</div>
