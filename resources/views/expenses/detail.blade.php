@extends('layouts.app', ['activeNav' => 'expenses'])

@php
  /** @var \App\Models\Expense $expense */
@endphp

@section('title', ucfirst($expense->description))

@section('content')

  <div class="container">
    <div class="row justify-content-center">

      <div class="col-sm-12 mb-3 px-3">
        <a type="button" class="btn rounded-pill btn-primary fw-bold mb-3"
           href="{{ url()->previous() === url()->current() ? route('expenses.index') : url()->previous() }}">
          <i class="material-icons float-start me-2">keyboard_backspace</i> {{ ucfirst(__('models.all.my.expenses')) }}
        </a>

        <div class="card shadow-sm">
          <div class="card-body">
            <div class="row">
              <div class="col-12 col-lg-7 mt-1 mt-lg-2">
                <h1 class="card-title fw-semi-bold">
                  {{ ucfirst(__('models.expense')) }}
                  <span class="featured fw-bold">{{ $expense->description }}</span>
                  {{ __('misc.of') }}
                  <span
                      class="featured fw-bold no-wrap">{{ \App\Utils\Utils::formatDate($expense->value_date) }}</span>
                </h1>
                <p class="card-text">
                  {{ ucfirst(__('misc.added.on.date.by', [
                    'date'=> \App\Utils\Utils::formatDate($expense->created_at),
                    'name' => $expense->creator->name
                  ])) }}
                </p>
              </div>
              <div class="col-12 col-lg-5 text-lg-end">
                <strong class="d-lg-block featured font-size-2em text-nowrap">
                  {{ \App\Utils\Utils::asPrice($expense->quotePart($user)) }}
                </strong>
                {{ __('misc.total.on.amount.of') }}
                <strong class="text-nowrap">{{ \App\Utils\Utils::asPrice($expense->amount) }}</strong>
              </div>
              <div class="col-12 mt-2">
                @if($expense->canBeConfirmedBy($user))
                  <a href="{{ route('expenses.confirm', ['id' => $expense->id]) }}"
                     class="btn btn-primary fw-bold mr-1">{{ __('misc.confirm') }}</a>
                @endif
                @if($expense->canBeDisputedBy($user))
                  <a href="{{ route('expenses.dispute', ['id' => $expense->id]) }}"
                     class="btn btn-danger fw-bold mr-1">{{ __('misc.dispute') }}</a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12 col-lg-6 mb-3 px-3">
        <h2 class="mt-2">{{ __('misc.who.participates') }}</h2>
        <div class="card shadow-sm mt-3 px-1 px-lg-3">
          <div class="table-responsive">
            <table class="table table-hover">
              <tbody>
              @foreach($expense->participants as $participant)
                <tr class="lh-large">
                  <td class="fw-bold">
                    {{ $participant->user->name }}
                  </td>
                  <td class="text-end fw-bold bigger">
                    {{ \App\Utils\Utils::asPrice($participant->amount) }}
                  </td>
                  <td class="bigger text-end">
                    {!! \App\Utils\ViewUtils::participantStatusBadge($participant) !!}
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="col-sm-12 col-lg-6 mb-3 px-3">
        <h2 class="mt-2">{{ __('misc.who.paid') }}</h2>
        <div class="card shadow-sm mt-3 px-1 px-lg-3">
          <div class="table-responsive">
            <table class="table table-hover">
              <tbody>
              @foreach($expense->payments as $payment)
                <tr class="lh-large">
                  <td class="fw-bold">
                    {{ $payment->user->name }}
                  </td>
                  <td class="text-end fw-bold bigger">
                    {{ \App\Utils\Utils::asPrice($payment->amount) }}
                  </td>
                  <td class="smaller text-end">
                    {{ \App\Utils\Utils::formatDate($payment->created_at ) }}
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
