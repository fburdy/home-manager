<h3 class="px-1 d-none d-sm-block mt-2">{{ ucfirst(__('models.transfer.add')) }}</h3>
<h1 class="fw-semi-bold px-3 d-block d-sm-none mt-3 mb-3">{{ ucfirst(__('models.transfer.add')) }}</h1>

<div class="card shadow-sm">
  <div class="card-body">
    <form method="post" action="{{ route('transfers.insert') }}">
      @csrf
      <select id="direction" name="direction" required
              class="form-select form-select-lg mb-3 {{ $errors->has('direction') ? 'is-invalid' : '' }}">
        <option value="received">{{ ucfirst(__('models.transfer.received.from')) }}...</option>
        <option value="sent">{{ ucfirst(__('models.transfer.sent.to')) }}...</option>
      </select>
      @if ($errors->has('direction'))
        <span class="invalid-feedback fw-bold">{{ $errors->first('direction') }}</span>
      @endif

      <select id="outsider_id" name="outsider_id" required
              class="form-select form-select-lg mb-3 {{ $errors->has('outsider_id') ? 'is-invalid' : '' }}">
        <option selected disabled value="">{{ ucfirst(__('misc.whom')) }}? ...</option>
        @foreach($outsiders as $outsider)
          <option value="{{ $outsider->id }}">#{{ $outsider->id}} &nbsp;{{ $outsider->name }}</option>
        @endforeach
      </select>
      @if ($errors->has('outsider_id'))
        <span class="invalid-feedback fw-bold">{{ $errors->first('outsider_id') }}</span>
      @endif

      <label for="amount">{{ ucfirst(__('misc.amount')) }}</label>
      <div class="input-group mb-3">
        <input type="number" step="0.01" min="0.01" id="amount" placeholder="{{ ucfirst(__('misc.amount.euros')) }}" name="amount" required
               class="form-control fw-bold form-control-lg {{ $errors->has('amount') ? 'is-invalid' : '' }}">
        <div class="input-group-text">€</div>
        @if ($errors->has('amount'))
          <span class="invalid-feedback fw-bold">{{ $errors->first('amount') }}</span>
        @endif
      </div>

      <label for="value_date">{{ ucfirst(__('misc.value.date')) }}</label>
      <div class="input-group mb-3">
        <input type="date" id="value_date" name="value_date" required
               class="form-control form-control-lg {{ $errors->has('value_date') ? 'is-invalid' : '' }}">
        <div class="input-group-text">
          <i class="far fa-calendar-alt"></i>
        </div>
        @if ($errors->has('value_date'))
          <span class="invalid-feedback fw-bold">{{ $errors->first('value_date') }}</span>
        @endif
      </div>

      <label for="description">{{ ucfirst(__('models.transfer.reason')) }}</label>
      <div class="input-group mb-3">
        <input type="text" id="description" placeholder="{{ ucfirst(__('models.transfer.desc.placeholder')) }}" name="description" required
               class="form-control form-control-lg {{ $errors->has('description') ? 'is-invalid' : '' }}">
        @if ($errors->has('description'))
          <span class="invalid-feedback fw-bold">{{ $errors->first('value_date') }}</span>
        @endif
      </div>

      <div class="row mb-1 mt-3">
        <div class="col-6">
          @if($cancelBtn ?? false)
            <a class="btn btn-secondary fw-bold" href="{{ route('transfers.index') }}">
              <i class="fas fa-arrow-left me-1"></i> {{ ucfirst(__('misc.cancel')) }}
            </a>
          @endif
        </div>
        <div class="col-6 text-end">
          <button type="submit" class="btn btn-primary fw-bold spin-submit">
            <i class="fas fa-check me-1"></i> {{ ucfirst(__('misc.save')) }}
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
