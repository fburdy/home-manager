@extends('layouts.app', ['activeNav' => 'transfers'])

@section('title', ucfirst(__('models.transfers.add')))

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-lg-5 p-0 px-lg-3">
        @include('transfers.transfer-form', ['cancelBtn' => true])
      </div>
    </div>
  </div>
@endsection
