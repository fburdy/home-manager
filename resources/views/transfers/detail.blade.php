@extends('layouts.app', ['activeNav' => 'transfers'])

@php
  /** @var \App\Models\Transfer $transfer */
@endphp

@section('title', ucfirst($transfer->description))

@section('content')

  <div class="container">
    <div class="row justify-content-center">

      <div class="col-sm-12 mb-3">
        <a type="button" class="btn rounded-pill btn-primary fw-bold mb-3"
           href="{{ url()->previous() === url()->current() ? route('transfers.index') : url()->previous() }}">
          <i class="material-icons float-start me-2">keyboard_backspace</i>
          {{ __('models.all.my.transfers') }}
        </a>
        <div class="card shadow">
          <div class="card-body">
            <div class="row">
              <div class="col-12 col-lg-7 mb-3">
                <h5 class="card-title mb-3">
                  {{ ucfirst(__('models.transfer')) }}
                  <span class="featured fw-bold">{{ $transfer->description }}</span>
                  {{ __('misc.from') }}
                  <span class="featured fw-bold">{{ \App\Utils\Utils::formatDate($transfer->value_date) }}</span>
                </h5>
                <p class="card-text fw-bold lh-large">
                  {{ $transfer->sender->name }}
                  <i class="material-icons large-text align-middle">arrow_forward</i> {{ $transfer->receiver->name }}
                </p>
                <p class="card-text smaller fw-semi-bold">
                  {{ ucfirst(__('misc.added.on.date.by', [
                    'date'=> \App\Utils\Utils::formatDate($transfer->created_at),
                    'name' => $transfer->creator->name
                  ])) }}
                </p>
              </div>
              <div class="col-12 col-lg-5 text-lg-end mb-3">
                <strong class="d-block featured font-size-2em text-nowrap">
                  {{ \App\Utils\Utils::asPrice($transfer->amount) }}
                </strong>
                {!! $transfer->getStatusBadge('large mt-2') !!}
              </div>
              <div class="col-12 text-end">
                @if($transfer->canBeDisputedBy($user))
                  <a href="{{ route('transfers.dispute', ['id' => $transfer->id]) }}"
                     class="btn btn-danger fw-bold mr-1">{{ __('misc.dispute') }}</a>
                @endif
                @if($transfer->canBeConfirmedBy($user))
                  <a href="{{ route('transfers.confirm', ['id' => $transfer->id]) }}"
                     class="btn btn-primary fw-bold mr-1">{{ __('misc.confirm') }}</a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
