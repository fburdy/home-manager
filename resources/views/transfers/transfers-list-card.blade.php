

<div class="row">
  <div class="col-6">
    <h3 class="px-1 d-none d-sm-block mt-1">{{ ucfirst(__('models.transfers')) }}</h3>
    <h1 class="fw-semi-bold px-3 d-block d-sm-none mt-1 mb-3">{{ ucfirst(__('models.transfers')) }}</h1>
  </div>
  <div class="col-6 d-lg-none text-end">
    <a class="btn btn-primary fw-bold me-1" href="{{ route('transfers.add') }}">
      <i class="fas fa-plus me-1"></i> {{ ucfirst(__('misc.new')) }}
    </a>
  </div>
</div>


<div class="card shadow-sm">
  <div class="table-responsive px-2">
    <table class="table table-hover">
      <thead class="thead-light">
      <tr>
        <th scope="col" class="text-center d-none d-xl-table-cell">{{ ucfirst(__('misc.ref')) }}</th>
        <th scope="col" class="text-center">Date</th>
        <th scope="col">{{ ucfirst(__('misc.from.de')) }}</th>
        <th scope="col">{{ ucfirst(__('misc.to')) }}</th>
        <th scope="col" class="text-end">{{ ucfirst(__('misc.amount')) }}</th>
        <th scope="col" class="text-center">{{ ucfirst(__('misc.status')) }}</th>
      </tr>
      </thead>
      <tbody>
      @php
        /** @var \App\Models\Transfer[]|\Illuminate\Database\Eloquent\Collection $transfers */
      @endphp
      @forelse($transfers as $transfer)
        <tr class="clickable-row {{ $transfer->isDisputed() ? 'table-contested' : '' }}"
            data-href="{{ route('transfers.detail', ['id' => $transfer->id]) }}">
          <td class="text-center smaller d-none d-xl-table-cell">
            <em>{{ $transfer->id }}</em>
          </td>
          <td class="smaller text-center">
            <span class="d-none d-xl-block">{{ \App\Utils\Utils::formatDate($transfer->value_date) }}</span>
            <span class="d-xl-none">{{ \App\Utils\Utils::formatDate($transfer->value_date, true) }}</span>
          </td>
          <td class="smaller">
            {{ $transfer->sender->name }}
          </td>
          <td class="smaller">
            {{ $transfer->receiver->name }}
          </td>
          <td class="text-end">
            <strong class="text-nowrap {{ $transfer->isReceived() ? 'text-success' : 'text-danger' }}">
              {{ \App\Utils\Utils::asPrice($transfer->amount) }}
            </strong>
          </td>
          <td class="bigger text-center">
            {!! $transfer->getStatusBadge() !!}
          </td>
        </tr>
      @empty
        <tr>
          <th scope="row" colspan="6" class="pt-4 text-center">
            {{ ucfirst(__('models.no.transfer.yet')) }}...
          </th>
        </tr>
      @endforelse
      </tbody>
    </table>
  </div>
  @if($transfers->isNotEmpty())
    <div class="card-footer pb-0">
      <div class="d-flex ms-auto fw-bold">
        {{ $transfers->onEachSide(1)->links() }}
      </div>
    </div>
  @endif
</div>
