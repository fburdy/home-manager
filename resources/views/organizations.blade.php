@extends('layouts.app', ['activeNav' => 'organisations'])

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-9">
        <div class="card shadow">
          <div class="card-header fw-bold">{{ ucfirst(__('models.organizations')) }}</div>
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success">
                {{ session('status') }}
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
