<div class="card shadow">
  <div class="card-header fw-bold">
    <i class="fas fa-briefcase"></i>
    {{ ucfirst(__('models.activity.add')) }}
  </div>
  <div class="card-body">
    <form method="post" action="{{ route('activity.insert') }}">
      @csrf
      <div class="form-group">
        <label for="name">{{ __('models.activity.name') }}</label>
        <div class="input-group mb-2">
          <input type="text" id="name" required
                 placeholder="&laquo; {{ \App\Utils\ViewUtils::randomActivityName() }} &raquo;" name="name"
                 class="form-control form-control-lg {{ $errors->has('name') ? 'is-invalid' : '' }}">
          @if ($errors->has('name'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('value_date') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="form-group">
        <label for="description">Description</label>
        <div class="input-group mb-2">
          <input type="text" id="description" placeholder="{{ __('misc.in.a.few.words') }}" name="description"
                 class="form-control form-control-lg {{ $errors->has('description') ? 'is-invalid' : '' }}">
          @if ($errors->has('description'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('value_date') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="form-group mb-0 text-end">
        <button type="submit" class="btn btn-primary fw-bold spin-submit">
          {{ ucfirst(__('misc.save')) }}
        </button>
      </div>
    </form>
  </div>
</div>
