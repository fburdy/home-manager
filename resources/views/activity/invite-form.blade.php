<form method="post" action="{{ route('activity.inviteUser') }}">
  @csrf
  <input type="hidden" name="activity_id" value="{{ $activity->id }}"/>
  <label for="invited_user" class="form-label fw-bold">Inviter un participant</label>
  <div class="input-group input-group-lg">
    <label class="input-group-text" for="invited_user">
      <i class="fas fa-user-plus"></i>
    </label>
    <select class="form-select form-select-lg " id="invited_user_id" name="invited_user_id" required>
      <option selected>Choisir...</option>
      @foreach($inviteUsers as $inviteUser)
        <option value="{{ $inviteUser->id }}">{{ $inviteUser->name }}</option>
      @endforeach
    </select>
    <button class="btn btn-primary fw-bold spin-submit" type="submit">
      <i class="fas fa-paper-plane"></i>
    </button>
  </div>
</form>
@if ($errors->any())
  @foreach ($errors->all() as $error)
    <div class="alert alert-danger mt-2">{{ $error }}</div>
  @endforeach
@endif
