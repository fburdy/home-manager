@extends('layouts.app', ['activeNav' => 'activity'])

@php
  /** @var \App\Models\Activity $activity */
@endphp

@section('title', ucfirst($activity->name))

@section('content')

  <div class="container">
    <div class="row justify-content-center">

      <div class="col-12">
        <a type="button" class="btn rounded-pill btn-primary fw-bold mb-3"
           href="{{ route('activity.index') }}">
          <i class="material-icons float-start me-2">keyboard_backspace</i> Tous mes activités
        </a>
      </div>

      <div class="col-lg-7 mb-3">
        <div class="card shadow">
          <div class="card-body">
            <div class="row">
              <div class="col col-12 col-lg-7">
                <h4 class="card-title fw-bold">
                  <i class="fas fa-briefcase mr-2"></i> {{ $activity->name }}
                </h4>
                <p>{{ $activity->description }}</p>
                <p class="card-text">
                  Créée le {{ \App\Utils\Utils::formatDate($activity->created_at) }}
                  par {{ $activity->owner->name }}
                </p>

                @if($waitingForMe !== null)
                  <a href="{{ route('activity.declineInvitation', ['token' => $waitingForMe->invitation_token]) }}"
                     class="btn btn-warning fw-bold text-white text-nowrap ms-1">Décliner l'invitation</a>
                  <a href="{{ route('activity.confirmInvitation', ['token' => $waitingForMe->invitation_token]) }}"
                     class="btn btn-primary fw-bold text-white text-nowrap ms-1">Accepter l'invitation</a>
                @endif

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-5 mb-3">
        <div class="card shadow mb-4">
          <div class="card-header fw-bold">
            Participants et invités
          </div>
          <div class="card-body pb-4">
            <span class="badge rounded-pill bg-primary large">
              <span class="smaller fw-bold">
                <i class="fas fa-user-check"></i>
                {{ $activity->activityUsers()->validated()->count() }} participants
              </span>
            </span>

            <span class="badge rounded-pill bg-warning large">
              <span class="smaller fw-bold">
                <i class="fas fa-user"></i>
                {{ $activity->activityUsers()->waitingValidation()->count() }} en attente
              </span>
            </span>

            <ul class="list-group mt-3 mb-3 max-h-500">
              @foreach( $activity->activityUsers()->get() as $activityUser)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  <div>
                    <i class="fas {{ $activityUser->isAdmin() ? 'fa-user-shield text-primary' : 'fa-user text-dark' }} mr-2"></i>
                    <span class="fw-bold">{{ $activityUser->user->name }}</span>
                    {{ $activityUser->user_id === $activity->owner_id ? '(créateur)' : '' }}
                  </div>
                  @if($activityUser->isValidated())
                    <span class="badge rounded-pill bg-primary fw-bold">validé</span>
                  @else
                    <span class="badge rounded-pill bg-warning fw-bold">en attente</span>
                  @endif
                </li>
              @endforeach
            </ul>

            @includeWhen($inviteUsers->count() > 0, 'activity.invite-form')
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection
