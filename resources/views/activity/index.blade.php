@extends('layouts.app', ['activeNav' => 'activity'])

@section('title', ucfirst(__('models.activities')))

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12 col-lg-4 mb-3 order-2 order-lg-1 p-0 px-lg-3">
        @include('activity.add-activity-form')
      </div>
      <div class="col-sm-12 col-lg-8 mb-3 order-1 order-lg-2 p-0 px-lg-3">
        @php
          /** @var \App\Models\Activity[]|\Illuminate\Support\Collection $activities */
        @endphp
        <div class="card shadow">
          <div class="card-header fw-bold">
            {{ trans_choice('models.participate.in.activities', $activities->count(), ['nb' => $activities->count()]) }}
          </div>
          <div class="card-body">
            <div class="row px-lg-3">

              @forelse($activities as $activity)
                <div class="col-md-6 px-1 mb-2">
                  <div class="card">
                    <div class="card-body shadow">
                      <h5>
                        <a href="{{ route('activity.detail', ['id'=>$activity->id]) }}" class="fw-bold">
                          {{ $activity->name }}
                        </a>
                      </h5>
                      <p>{{ $activity->description }}</p>

                      <span class="badge rounded-pill bg-primary large mb-1">
                        <span class="smaller fw-bold">
                          <i class="fas fa-user-check"></i>
                          {{ $activity->activityUsers()->validated()->count() }} participants
                        </span>
                      </span>
                      <span class="badge rounded-pill bg-warning large mb-1">
                        <span class="smaller fw-bold">
                          <i class="fas fa-user"></i>
                          {{ $activity->activityUsers()->waitingValidation()->count() }} en attente
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
              @empty
                <p>Aucune activité pour le moment...</p>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
