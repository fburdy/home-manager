<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="{{ asset('img/logo/logo-192.png') }}"/>
  <title>HomeManager</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="welcome-page">

<div class="flex-center flex-column position-ref full-height">

  <div class="content">
    <div class="title mb-4 uppercase">
      <img src="{{ asset('img/logo/logo-90.png') }}" alt=""/>
      Home<span class="featured">Manag€r</span>
    </div>
  </div>

  @if (Route::has('login'))
    <div class="fw-bold">
      @auth
        <a href="{{ route('dashboard.index') }}" class="btn btn-lg rounded-pill btn-secondary m-2">
          {{ __('misc.use.app') }}
        </a>
      @else
        <a href="{{ route('login') }}" class="btn btn-lg rounded-pill btn-secondary m-2">{{ __('auth.sign.in') }}</a>
        <a href="{{ route('register') }}" class="btn btn-lg rounded-pill btn-primary m-2">{{ __('auth.register') }}</a>
      @endauth
    </div>
  @endif

  <div class="links fw-bold mt-5 text-center">
    <a href="#" class="mb-2">{{ __('misc.about') }}</a>
    <a href="#" class="mb-2">GitHub</a>
    <a href="#" class="mb-2">Support</a>
    <span class="fw-semi-bold">by <a href="https://github.com/francoisburdy">François Burdy</a></span>
  </div>

</div>

</body>
</html>
