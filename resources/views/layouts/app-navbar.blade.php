<nav class="navbar fixed-top shadow navbar-expand-lg navbar-light navbar-laravel bg-light">
  <div class="container">
    <a class="navbar-brand uppercase" href="{{ route('dashboard.index') }}">
      <img src="{{ asset('img/logo/logo-90.png') }}" style="width: 18px"/>
      <span class="d-lg-inline-block fw-semi-bold">HOME <span class="featured">MANAG€R</span></span>
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav nav-pills ms-auto">
        @guest
          <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">{{ __('auth.sign.in') }}</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">{{ __('auth.register') }}</a></li>
        @else
          <li class="nav-item">
            <a class="nav-link {{ ($activeNav ?? null) === 'dashboard' ? 'active' : '' }}"
               href="{{ route('dashboard.index') }}">
              {{ ucfirst(__('models.dashboard')) }}
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ ($activeNav ?? null) === 'expenses' ? 'active' : '' }}"
               href="{{ route('expenses.index') }}">
              <i class="fas fa-shopping-cart"></i> {{ ucfirst(__('models.expenses')) }}
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ ($activeNav ?? null) === 'transfers' ? 'active' : '' }}"
               href="{{ route('transfers.index') }}">
              <i class="fas fa-exchange-alt"></i> {{ ucfirst(__('models.transfers')) }}
            </a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link {{ ($activeNav ?? null) === 'organisations' ? 'active' : '' }}"
               href="{{ route('organisations.index') }}">
              {{ ucfirst(__('models.organizations')) }}
            </a>
          </li> --}}
          <li class="nav-item dropdown">
            <a id="navbarDropdown" href="#"
               class="nav-link dropdown-toggle {{ ($activeNav ?? null) === 'activity' ? 'active' : '' }}"
               role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-briefcase"></i>
              {{ \App\Utils\Utils::truncate($currentActivity->name ?? "Pas d'activité", 15) }}
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item" href="{{ route('activity.index') }}">Gérer mes activités</a>
              </li>
              <li><hr class="dropdown-divider"></li>
              @foreach($validatedActivities as $activity)
                <li>
                  @php
                    $isActiveActivity = $currentActivity->id === $activity->id;
                  @endphp
                  <a class="dropdown-item {{ $isActiveActivity ? 'disabled' :'' }}"
                     href="{{ route('activity.switch', ['id' => $activity->id]) }}">
                    @if($isActiveActivity)
                      <i class="fas fa-chevron-right"></i>
                    @endif
                    {{ $activity->name }}
                  </a>
                </li>
              @endforeach
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"
               class="nav-link dropdown-toggle {{ ($activeNav ?? null) === 'account' ? 'active' : '' }}">
              <i class="fas fa-user-circle"></i> {{ auth()->user()->name }} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item" href="{{ route('user.profile') }}">{{ __('auth.user.account') }}</a>
              </li>
              <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  {{ __('auth.sign.out') }}
                </a>
              </li>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
              </form>
            </ul>
          </li>
        @endguest
      </ul>
    </div>
  </div>
</nav>
