<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="{{ asset('img/logo/logo-90.png') }}"/>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title') | {{ config('app.name', 'Home Manager') }}</title>
  <link rel="manifest" href="{{ route('manifest') }}">
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
  @include('layouts.app-navbar')
  <div class="container pt-3 pt-lg-4">
    <div class="row justify-content-center">
      <div class="col-12">
        @include('layouts.app-session-alerts')
      </div>
    </div>
  </div>

  <main class="pt-1 pb-5">
    @yield('content')
  </main>
</div>
<script src="{{ asset('js/app.js') }}" defer></script>
@yield('scripts')
</body>
</html>
