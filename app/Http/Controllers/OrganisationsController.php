<?php

namespace App\Http\Controllers;

class OrganisationsController extends Controller
{

  /**
   * Show the organizations page
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('organizations');
  }
}
