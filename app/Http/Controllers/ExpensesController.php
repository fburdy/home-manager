<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use App\Models\ExpenseParticipant;
use App\Models\ExpenseType;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class ExpensesController extends Controller
{
  /**
   * Show the expenses page
   *
   * @param Request $request
   * @return Factory|View
   */
  public function index(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    $expenses = Expense::concernUser($user)
        ->orderBy('expenses.value_date', 'desc')
        ->orderBy('expenses.created_at', 'desc')
        ->take(10000)
        ->paginate(30);

    return view('expenses.index')
        ->with('outsiders', User::where('id', '<>', $user->id)->get())
        ->with('expenseTypes', ExpenseType::root()->get())
        ->with('expenses', $expenses);
  }

  /**
   * Show the new expense page
   *
   * @param Request $request
   * @return Factory|View
   */
  public function add(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    return view('expenses.add')
        ->with('outsiders', User::where('id', '<>', $user->id)->get())
        ->with('expenseTypes', ExpenseType::root()->get());
  }

  public function detail(Request $request, int $id)
  {
    /** @var User $user */
    $user = $request->user();
    $expense = Expense::find($id);

    if ($expense === null) {
      $request->session()->flash('error', ucfirst(__('models.expense.not.found')));
      return redirect()->route('expenses.index');
    }

    if (!$expense->isUserAllowed($user)) {
      $request->session()->flash('error', ucfirst(__('models.expense.not.involved')));
      return redirect()->route('expenses.index');
    }

    return view('expenses.detail')->with('expense', $expense);
  }

  public function dispute(Request $request, int $id)
  {
    /** @var User $user */
    $user = $request->user();
    $expense = Expense::find($id);

    if ($expense === null) {
      $request->session()->flash('error', ucfirst(__('models.expense.not.found')));
      return redirect()->route('expenses.index');
    }

    if (!$expense->isUserAllowed($user)) {
      $request->session()->flash('error', ucfirst(__('models.expense.not.involved')));
      return redirect()->route('expenses.index');
    }

    if (!$expense->canBeDisputedBy($request->user())) {
      $request->session()->flash('error', ucfirst(__('models.expense.already.confirmed')));
      return back();
    }

    /** @var ExpenseParticipant $participant */
    $participant = $expense->participants()->where('user_id', $user->id)->first();
    $participant->state = ExpenseParticipant::STATUS_DISPUTED;
    $participant->save();

    $request->session()->flash('info', ucfirst(__('models.expense.disputed', ['name' => $expense->name])));

    return back();
  }

  public function confirm(Request $request, int $id)
  {
    /** @var User $user */
    $user = $request->user();
    $expense = Expense::find($id);

    if ($expense === null) {
      $request->session()->flash('error', ucfirst(__('models.expense.not.found')));
      return redirect()->route('expenses.index');
    }

    if (!$expense->isUserAllowed($user)) {
      $request->session()->flash('error', ucfirst(__('models.expense.not.involved')));
      return redirect()->route('expenses.index');
    }

    if (!$expense->canBeConfirmedBy($request->user())) {
      $request->session()->flash('error', ucfirst(__('models.expense.already.confirmed')));
      return back();
    }

    /** @var ExpenseParticipant $participant */
    $participant = $expense->participants()->where('user_id', $user->id)->first();
    $participant->state = ExpenseParticipant::STATUS_CONFIRMED;
    $participant->save();

    $request->session()->flash('success', ucfirst(__('models.expense.confirmed', ['name' => $expense->name])));
    return back();
  }

  /**
   * @param Request $request
   * @return RedirectResponse
   */
  public function insert(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    $validator = Validator::make($request->all(), [
        'description' => 'required|string|max:255',
        'amount' => 'required|numeric|min:0|max:1000000',
        'expense_type' => 'required|integer|exists:expense_types,id',
        'activity_id' => 'required|integer|exists:activity,id',
        'value_date' => 'date_format:Y-m-d',
        'participants' => 'array|required',
        'participants.*.user_id' => 'required|integer|exists:users,id',
        'participants.*.share_amount' => 'nullable|numeric',
        'participants.*.paid_amount' => 'nullable|numeric',
    ]);

    $amount = abs((float) $request->get('amount'));
    $participants = $request->get('participants');
    $customShareAmount = false;
    $customPaidAmount = false;
    $sumPaid = 0.0;
    $sumShare = 0.0;
    $nbActiveParticipants = 0;

    foreach ($participants as $participant) {
      if (isset($participant['active']) && $participant['active'] === 'on') {
        $nbActiveParticipants++;
        if ($participant['share_amount'] !== null) {
          $customShareAmount = true;
          $sumShare += $participant['share_amount'];
        }
        if ($participant['paid_amount'] !== null) {
          $customPaidAmount = true;
          $sumPaid += $participant['paid_amount'];
        }
      }
    }

    $validator->after(function (\Illuminate\Validation\Validator $validator) use ($customShareAmount, $sumShare, $amount) {
      if ($customShareAmount && $sumShare > $amount) {
        $validator->errors()->add('participants', 'Somme des quote-parts supérieure au total de la dépense.');
      }
    })->after(function (\Illuminate\Validation\Validator $validator) use ($customPaidAmount, $sumPaid, $amount) {
      if ($customPaidAmount && $sumPaid !== $amount) {
        $validator->errors()->add('participants', 'Somme des paiements différente du total de la dépense.');
      }
    });

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $expense = Expense::create([
        'description' => trim($request->get('description')),
        'amount' => $amount,
        'value_date' => $request->get('value_date') ?? date('Y-m-d'),
        'activity_id' => $request->get('activity_id')
    ]);

    $expense->creator()->associate($user);
    $expense->expenseType()->associate(ExpenseType::find($request->get('expense_type')));
    $expense->save();

    foreach ($participants as $participant) {
      if (isset($participant['active']) && $participant['active'] === 'on') {
        ExpenseParticipant::create([
            'amount' => $customShareAmount ? ($participant['share_amount'] ?? 0.0) : $amount / $nbActiveParticipants,
            'state' => ExpenseParticipant::STATUS_WAITING_VALIDATION,
            'user_id' => $participant['user_id'],
            'expense_id' => $expense->id
        ]);

        if (!$customPaidAmount || $participant['paid_amount'] > 0.0) {
          Payment::create([
              'amount' => $customPaidAmount ? $participant['paid_amount'] : $amount / $nbActiveParticipants,
              'payment_date' => now(),
              'payer_id' => $participant['user_id'],
              'expense_id' => $expense->id
          ]);
        }
      }
    }
    $request->session()->flash('success', ucfirst(__('models.expense.saved')));
    return back();
  }
}
