<?php

namespace App\Http\Controllers;

class PublicController extends Controller
{
  public function welcome()
  {
    return view('welcome');
  }

  public function manifest()
  {
    return response()->json([
        'short_name' => 'HomeManager',
        'name' => 'Home Manager',
        'icons' => [
            [
                'src' => '/img/logo/logo-192.png',
                'type' => 'image/png',
                'sizes' => '192x192'
            ],
        ],
        'start_url' => '/dashboard',
        'background_color' => "#3367D6",
        'display' => 'standalone',
        'scope' => '/dashboard',
        'theme_color' => '#3367D6'
    ]);
  }
}
