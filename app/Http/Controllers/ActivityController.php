<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\ActivityUser;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class ActivityController extends Controller
{
  /**
   * @param Request $request
   * @return Factory|View
   */
  public function index(Request $request)
  {
    /** @var User $user */
    $user = $request->user();

    return view('activity.index')
        ->with('activities', $user->getActivities());
  }

  public function insert(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    $request->validate([
        'name' => 'required|string|max:60',
        'description' => 'nullable|string|max:200',
    ]);

    $activity = Activity::create([
        'name' => trim($request->get('name')),
        'description' => trim($request->get('description')),
        'owner_id' => $user->id,
    ]);

    $activity->inviteUser($user, User::ROLE_ADMIN, true);
    return redirect()->route('activity.detail', ['id' => $activity->id]);
  }


  public function detail(Request $request, $id)
  {
    /** @var User $user */
    $user = $request->user();
    $activity = Activity::find($id);

    if ($activity === null) {
      $request->session()->flash('error', 'Activité introuvable.');
      return redirect()->route('activity.index');
    }

    if (!$activity->hasUser($user)) {
      $request->session()->flash('error', 'Vous n\'êtes pas concerné par cette activité.');
      return redirect()->route('activity.index');
    }

    $inviteUsers = User::whereNotIn('id', $activity->activityUsers()->pluck('user_id'))->get();

    return view('activity.detail')
        ->with('activity', $activity)
        ->with('inviteUsers', $inviteUsers)
        ->with('waitingForMe', $activity->waitingForUser($user));
  }

  public function update(Request $request, $id)
  {
    //
  }

  /**
   * /!\ This route is not authenticated
   * @param Request $request
   * @param array $token
   * @return \Illuminate\Http\RedirectResponse
   */
  public function confirmInvitation(Request $request, $token)
  {
    $activityUser = ActivityUser::where('invitation_token', $token)->whereNull('validation_date')->first();
    if ($activityUser === null)
      abort(Response::HTTP_UNPROCESSABLE_ENTITY, "Ce lien n'est plus valide.");

    $activityUser->validation_date = now();
    $activityUser->save();

    if (!auth()->check())
      auth()->login($activityUser->user);

    $request->session()->flash('success', "Invitation acceptée dans l'activité {$activityUser->activity->name}");

    return redirect()->route('dashboard.index');
  }

  /**
   * /!\ This route is not authenticated
   * @param Request $request
   * @param array $token
   * @return \Illuminate\Http\RedirectResponse
   */
  public function declineInvitation(Request $request, $token)
  {
    $activityUser = ActivityUser::where('invitation_token', $token)->whereNull('validation_date')->first();
    if ($activityUser === null)
      abort(Response::HTTP_UNPROCESSABLE_ENTITY, "Ce lien n'est plus valide.");

    $activityUser->delete();

    if (!auth()->check())
      auth()->login($activityUser->user);

    $request->session()->flash('info', "Invitation dans l'activité {$activityUser->activity->name} déclinée.");

    return redirect()->route('activity.index');
  }

  public function inviteUser(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    $request->validate([
        'activity_id' => 'required|integer|exists:activity,id',
        'invited_user_id' => 'required|integer|exists:users,id',
    ]);

    $activity = Activity::find($request->get('activity_id'));

    if (!$activity->hasUser($user, true)) {
      $request->session()->flash('error', 'Vous n\'êtes pas autorisé à inviter des participants.');
      return back();
    }

    $inviteUser = User::find($request->get('invited_user_id'));
    if ($activity->hasUser($inviteUser)) {
      $request->session()->flash('error', 'Cet utilisateur est déjà invité à participer.');
      return back();
    }

    $activity->inviteUser($inviteUser, User::ROLE_PARTICIPANT, false);
    return redirect()->route('activity.detail', ['id' => $activity->id]);
  }

  public function switch(Request $request, $id)
  {
    /** @var User $user */
    $user = $request->user();
    $activity = Activity::find($id);

    if ($activity === null) {
      $request->session()->flash('error', 'Activité introuvable.');
      return back();
    }

    if (!$activity->hasValidatedUser($user)) {
      $request->session()->flash('error', 'Vous n\'êtes pas autorisé à accéder à cette activité.');
      return back();
    }

    $user->current_activity_id = $activity->id;
    $user->save();
    return back();
  }

}
