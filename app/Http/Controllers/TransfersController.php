<?php

namespace App\Http\Controllers;

use App\Models\Transfer;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TransfersController extends Controller
{
  /**
   * Show the transfers page
   * @param Request $request
   * @return Factory|View
   */
  public function index(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    $transfers = $user->allTransfers()
        ->orderBy('value_date', 'desc')
        ->take(10000)
        ->paginate(30);
    return view('transfers.index')
        ->with('outsiders', User::where('id', '<>', $user->id)->get())
        ->with('transfers', $transfers);
  }

  /**
   * Show the new transfer page
   * @param Request $request
   * @return Factory|View
   */
  public function add(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    return view('transfers.add')
        ->with('outsiders', User::where('id', '<>', $user->id)->get());
  }

  public function detail(Request $request, int $id)
  {
    /** @var User $user */
    $user = $request->user();
    $transfer = Transfer::find($id);
    if ($transfer === null) {
      $request->session()->flash('error', 'Versement introuvable.');
      return redirect()->route('transfers.index');
    }
    if (!$transfer->isUserAllowed($user)) {
      $request->session()->flash('error', 'Vous n\'êtes pas concerné par cette dépense.');
      return redirect()->route('transfers.index');
    }
    return view('transfers.detail')->with('transfer', $transfer);
  }

  /**
   * @param Request $request
   * @return RedirectResponse
   */
  public function insert(Request $request)
  {
    /** @var User $user */
    $user = $request->user();
    $request->validate([
        'description' => 'required|string|max:255',
        'amount' => 'required|numeric|min:0|max:1000000',
        'outsider_id' => 'required|integer|exists:users,id',
        'value_date' => 'date_format:Y-m-d',
        'direction' => 'required|string|in:sent,received'
    ]);
    /** @var User $outsider */
    $outsider = User::find($request->get('outsider_id'));
    $received = $request->get('direction') === 'received';
    $amount = abs((float) $request->get('amount'));
    // check outsider !== user
    $transfer = Transfer::create([
        'description' => trim($request->get('description')),
        'state' => $received ? Transfer::STATUS_CONFIRMED : Transfer::STATUS_WAITING,
        'creator_id' => $request->user()->id,
        'from_user_id' => $received ? $outsider->id : $user->id,
        'to_user_id' => $received ? $user->id : $outsider->id,
        'amount' => $amount,
        'value_date' => $request->get('value_date') ?? date('Y-m-d'),
    ]);
    if ($received)
      $request->session()->flash('success', 'Versement enregistré');
    else
      $request->session()->flash('success', 'Versement enregistré en attente de confirmation');
    return redirect()->route('transfers.index');
  }

  public function dispute(Request $request, int $id)
  {
    /** @var User $user */
    $user = $request->user();
    $transfer = Transfer::find($id);
    if ($transfer === null) {
      $request->session()->flash('error', 'Versement introuvable.');
      return redirect()->route('transfers.index');
    }
    if ($transfer->isConfirmed()) {
      $request->session()->flash('error', 'Vous avez déjà confirmé ce versement.');
      return redirect()->route('transfers.detail', ['id' => $transfer->id]);
    }
    if ($transfer->isDisputed()) {
      $request->session()->flash('info', 'Vous avez déjà contesté ce versement.');
      return redirect()->route('transfers.detail', ['id' => $transfer->id]);
    }
    if (!$transfer->canBeDisputedBy($user)) {
      $request->session()->flash('error', 'Vous ne pouvez pas contester ce versement.');
      return redirect()->route('transfers.index');
    }
    $transfer->state = Transfer::STATUS_DISPUTED;
    $transfer->save();
    $request->session()->flash('info', 'Versement contesté.');
    return back();
  }

  public function confirm(Request $request, int $id)
  {
    /** @var User $user */
    $user = $request->user();
    $transfer = Transfer::find($id);
    if ($transfer === null) {
      $request->session()->flash('error', 'Versement introuvable.');
      return redirect()->route('transfers.index');
    }
    if ($transfer->isConfirmed()) {
      $request->session()->flash('info', 'Vous avez déjà confirmé ce versement.');
      return back();
    }
    if (!$transfer->canBeConfirmedBy($user)) {
      $request->session()->flash('error', 'Vous ne pouvez pas confirmer ce versement.');
      return back();
    }
    $transfer->state = Transfer::STATUS_CONFIRMED;
    $transfer->save();
    $request->session()->flash('success', "Versement {$transfer->id} confirmé.");
    return back();
  }
}
