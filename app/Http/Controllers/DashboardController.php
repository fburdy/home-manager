<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

  /**
   * Show the application dashboard.
   * @param Request $request
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index(Request $request)
  {
    /** @var User $user */
    $user = $request->user();

    return view('dashboard.index')
        ->with('debtsBalance', $user->debtsBalance())
        ->with('waitingExpenseParticipants', $user->getWaitingExpenseParticipants())
        ->with('waitingTransfers', $user->getWaitingTransfers())
        ->with('waitingUserActivities', $user->activityUsers()->waitingValidation()->get());
  }
}
