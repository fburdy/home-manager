<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;


class UserController extends Controller
{
  /**
   * Show the user profile page
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function profile()
  {
    return view('auth.profile')
        ->with('user', Auth::user());
  }

  /**
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function update(Request $request)
  {
    /** @var User $user */
    $user = $request->user();

    $request->validate([
        'name' => 'required|string|max:255',
        'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
    ]);

    $user->fill([
        'name' => trim($request->get('name')),
        'email' => trim($request->get('email')),
    ])->save();

    $request->session()->flash('success', 'Informations utilisateur enregistrées');
    return redirect()->route('user.profile');
  }

  /**
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function changePassword(Request $request)
  {
    $request->validate([
        'password' => 'required|string|min:8|confirmed',
        'current-password' => 'required|string'
    ]);
    /** @var User $user */
    $user = $request->user();

    if (!Hash::check($request->get('current-password'), $user->getAuthPassword())) {
      $request->session()->flash('error', 'Mot de passe actuel incorrect. Veuillez réessayer.');
      return redirect()->route('user.profile');
    }

    $user->fill([
        'password' => Hash::make($request->get('password'))
    ])->save();

    $request->session()->flash('success', 'Nouveau mot de passe enregistré');
    return redirect()->route('user.profile');
  }

}
