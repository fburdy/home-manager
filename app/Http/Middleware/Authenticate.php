<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class Authenticate extends \Illuminate\Auth\Middleware\Authenticate
{
  public function handle($request, Closure $next, ...$guards)
  {
    $this->authenticate($request, $guards);

    if (auth()->check()) {
      /** @var User $user */
      $user = auth()->user();
      view()->share('user', $user);
      view()->share('currentActivity', $user->getCurrentActivity());
      view()->share('validatedActivities', $user->validatedActivities());
    }
    return $next($request);
  }

}
