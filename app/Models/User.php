<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

/**
 * Class User
 * @package App\Models
 * @mixin Builder
 * @property int id
 * @property string name
 * @property string email
 * @property string password
 * @property string remember_token
 * @property string created_at
 * @property string updated_at
 * @property int current_activity_id
 * @property Payment[]|Collection payments
 * @property Transfer[]|Collection receivedTransfers
 * @property Transfer[]|Collection sentTransfers
 * @property ActivityUser[]|Collection activityUsers
 * @property Activity currentActivity
 * @property ExpenseParticipant[]|Collection expenseParticipants
 */
class User extends Authenticatable
{
  use Notifiable;

  protected $table = 'users';

  public const ROLE_PARTICIPANT = 10;
  public const ROLE_ADMIN = 20;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'email', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password', 'remember_token',
  ];

  /**
   * Get the transfers the user has received
   */
  public function receivedTransfers()
  {
    return $this->hasMany(Transfer::class, 'to_user_id');
  }

  /**
   * Get the transfers the user has received
   */
  public function sentTransfers()
  {
    return $this->hasMany(Transfer::class, 'from_user_id');
  }

  public function payments()
  {
    return $this->hasMany(Payment::class, 'payer_id');
  }

  public function expenseParticipants()
  {
    return $this->hasMany(ExpenseParticipant::class, 'user_id');
  }

  public function activityUsers()
  {
    return $this->hasMany(ActivityUser::class, 'user_id');
  }

  public function currentActivity()
  {
    return $this->belongsTo(Activity::class, 'current_activity_id');
  }

  public function hasActivity(Activity $activity): bool
  {
    return $activity->hasUser($this);
  }

  public function hasValidatedActivity(Activity $activity): bool
  {
    return $activity->hasValidatedUser($this);
  }

  public function allTransfers()
  {
    $user = $this;
    return Transfer::where(function (Builder $q) use ($user) {
      $q->where('from_user_id', $user->id)->orWhere('to_user_id', $user->id);
    });
  }

  /**
   * Return the list of related users, who are subject to debts with the current user
   * @return Collection|User[]
   */
  public function relatedUsers(): Collection
  {
    $users = collect();
    foreach ($this->payments as $payment) {
      foreach ($payment->expense->participants as $participant) {
        $user = $participant->user;
        if (!$users->contains($user)) $users->add($user);
      }
    }

    foreach ($this->receivedTransfers as $transfer) {
      $sender = $transfer->sender;
      if (!$users->contains($sender)) $users->add($sender);
    }

    foreach ($this->sentTransfers as $transfer) {
      $receiver = $transfer->receiver;
      if (!$users->contains($receiver)) $users->add($receiver);
    }

    return $users->reject(function (User $user) {
      return $user->id === $this->id;
    });
  }

  /**
   * @param User $other
   * @return array
   * @todo test coverage
   */
  public function getBalanceWith(User $other)
  {
    $balanceItems = [
        'user' => $other,
        'expenses' => [],
        'transfers' => [],
        'balanceExpenses' => 0.0,
        'balanceTransfers' => 0.0,
        'balanceTotal' => 0.0,
    ];

    // balance with ourself is always null
    if ($this->id === $other->id)
      return $balanceItems;

    // Compute common expenses balance
    /** @var Expense[]|Collection $expenses */
    $expenses = Expense::onlyConfirmed()->concernAll([$this, $other])->get();
    foreach ($expenses as $expense) {
      /** @var Payment $paymentByOther */
      $paymentByOther = $expense->payments()->where('payer_id', $other->id)->first();
      if ($paymentByOther !== null) {
        $consumedByMe = $expense->participants()->where('user_id', $this->id)->select('amount')->first();
        if ($consumedByMe !== null) {
          $balanceItems['expenses'][] = [
              'expense' => $expense,
              'balance' => -1 * $paymentByOther->amount * $consumedByMe->amount / $expense->amount,
          ];
        }
      }
      /** @var Payment $paymentByOther */
      $paymentByMe = $expense->payments()->where('payer_id', $this->id)->first();
      if ($paymentByMe !== null) {
        $consumedByOther = $expense->participants()->where('user_id', $other->id)->select('amount')->first();
        if ($consumedByOther !== null) {
          $balanceItems['expenses'][] = [
              'expense' => $expense,
              'balance' => $paymentByMe->amount * $consumedByOther->amount / $expense->amount,
          ];
        }
      }
    }

    // Compute transfers balance
    /** @var Transfer[]|Collection $transfers */
    $transfers = Transfer::onlyConfirmed()->betweenUsers($this, $other)->get();
    foreach ($transfers as $transfer) {
      $balanceItems['transfers'][] = [
          'transfer' => $transfer,
          'balance' => $transfer->amount * ($transfer->receiver->id === $this->id ? -1 : 1),
      ];
    }

    // Sums up items & compute total balance
    foreach ($balanceItems['expenses'] as $item) {
      $balanceItems['balanceExpenses'] += $item['balance'];

      $balanceItems['balanceTotal'] += $item['balance'];
    }
    foreach ($balanceItems['transfers'] as $item) {
      $balanceItems['balanceTransfers'] += $item['balance'];
      $balanceItems['balanceTotal'] += $item['balance'];
    }
    return $balanceItems;
  }

  public function debtsBalance(): array
  {
    $relatedUsers = $this->relatedUsers();
    $res = [
        'members' => [],
        'totalPayable' => 0.0,
        'totalReceivable' => 0.0,
        'totalBalance' => 0.0,
    ];

    foreach ($relatedUsers as $other) {
      $balance = $this->getBalanceWith($other);
      $res['members'][$other->id] = $balance;

      if ($balance['balanceTotal'] < 0.0) {
        $res['totalPayable'] += -1 * $balance['balanceTotal'];
      } else if ($balance['balanceTotal'] > 0.0) {
        $res['totalReceivable'] += $balance['balanceTotal'];
      }
    }

    $res['totalBalance'] = $res['totalReceivable'] - $res['totalPayable'];
    return $res;
  }

  public function getWaitingExpenseParticipants()
  {
    return $this->expenseParticipants()->waiting()->get();
  }

  public function getWaitingTransfers()
  {
    return $this->receivedTransfers()->waiting()->get();
  }

  public function getCurrentActivity()
  {
    if ($this->currentActivity !== null)
      return $this->currentActivity;

    if ($this->validatedActivities()->isNotEmpty())
      return $this->validatedActivities()->last();

    if ($this->getActivities()->isNotEmpty())
      return $this->getActivities()->last();

    return null;
  }

  /**
   * @return Activity[]|Collection
   */
  public function getActivities(): Collection
  {
    $activities = collect();
    foreach ($this->activityUsers as $activityUser) {
      $activities->add($activityUser->activity);
    }
    return $activities;
  }

  public function validatedActivities(): Collection
  {
    $activities = collect();
    foreach ($this->activityUsers as $activityUser) {
      if ($activityUser->validation_date !== null) {
        $activities->add($activityUser->activity);
      }
    }
    return $activities;
  }
}
