<?php

namespace App\Models;

use App\Utils\Utils;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Activity
 * @package App\Models
 * @property int id
 * @property string name
 * @property string description
 * @property int owner_id
 * @property string created_at
 * @property string updated_at
 * @mixin Builder
 */
class Activity extends Model
{
  protected $table = 'activity';

  protected $guarded = ['id'];

  public function expenses()
  {
    return $this->hasMany(Expense::class, 'activity_id');
  }

  public function transfers()
  {
    return $this->hasMany(Transfer::class, 'activity_id');
  }

  public function owner()
  {
    return $this->belongsTo(User::class, 'owner_id');
  }

  public function activityUsers()
  {
    return $this->hasMany(ActivityUser::class, 'activity_id');
  }

  /**
   * @param User $user
   * @param int $role
   * @param bool $validate
   * @return ActivityUser|Model
   * @throws \Exception
   * @todo send invite email
   */
  public function inviteUser(User $user, $role = User::ROLE_PARTICIPANT, $validate = false)
  {
    return ActivityUser::create([
        'user_id' => $user->id,
        'activity_id' => $this->id,
        'role' => $role,
        'invitation_token' => Utils::randomToken(50),
        'validation_date' => $validate ? now() : null,
    ]);
  }

  public function hasValidatedUser(User $user, $onlyAdmin = false): bool
  {
    $query = $this->activityUsers()->validated()->where('user_id', $user->id);
    if ($onlyAdmin) $query->where('role', User::ROLE_ADMIN);
    return $query->exists();
  }

  public function hasUser(User $user, $onlyAdmin = false)
  {
    $query = $this->activityUsers()->where('user_id', $user->id);
    if ($onlyAdmin) $query->where('role', User::ROLE_ADMIN);
    return $query->exists();
  }

  public function waitingForUser(User $user): ?ActivityUser
  {
    return $this->activityUsers()->where('user_id', $user->id)->whereNull('validation_date')->first();
  }
}
