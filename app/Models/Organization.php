<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Organization
 * @package App\Models
 * @mixin Builder
 */
class Organization extends Model
{
  protected $table = 'organizations';

}
