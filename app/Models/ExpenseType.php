<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ExpenseType
 * @package App\Models
 * @mixin Builder
 */
class ExpenseType extends Model
{
  protected $table = 'expense_types';

  public function children()
  {
    return $this->hasMany(ExpenseType::class, 'parent_id');
  }

  public function hasChildren(): bool
  {
    return $this->children !== null && $this->children->count() > 0;
  }

  public function parent()
  {
    return $this->belongsTo(ExpenseType::class, 'parent_id', 'id');
  }

  public function scopeRoot(Builder $query)
  {
    return $query->whereNull('parent_id');
  }

}
