<?php

namespace App\Models;

use App\Models\Interfaces\Disputable;
use Barryvdh\Reflection\DocBlock\Type\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Expense
 * @package App\Models
 * @property int id
 * @property float amount
 * @property int creator_id
 * @property int|null expense_type
 * @property string|null description
 * @property string value_date
 * @property string created_at
 * @property string updated_at
 * @property int organization_id
 * @property int activity_id
 * @property ExpenseParticipant[]|Collection participants
 * @property Payment[]|Collection payments
 * @property User creator
 * @property Activity activity
 * @property ExpenseType expenseType
 * @mixin Builder
 */
class Expense extends Model implements Disputable
{
  public const STATUS_WAITING_VALIDATION = 0;
  public const STATUS_CONFIRMED = 5;
  public const STATUS_DISPUTED = 10;

  protected $table = 'expenses';

  protected $fillable = [
      'description',
      'amount',
      'value_date',
      'expense_type',
      'activity_id',
  ];

  public function participants()
  {
    return $this->hasMany(ExpenseParticipant::class, 'expense_id');
  }

  public function scopeOnlyConfirmed(Builder $query): Builder
  {
    return $query->whereDoesntHave('participants', function (Builder $query) {
      $query->where('state', '!=', ExpenseParticipant::STATUS_CONFIRMED);
    });
  }

  public function scopeConcernUser(Builder $query, User $user)
  {
    return $query->whereHas('participants', function (Builder $query) use ($user) {
      $query->where('user_id', $user->id);
    });
  }

  public function scopeConcernAll(Builder $query, array $users = [])
  {
    /** @var User[] $users */
    foreach ($users as $user) {
      $query->whereHas('participants', function (Builder $query) use ($user) {
        $query->where('user_id', $user->id);
      });
    }
    return $query;
  }

  public function isUserAllowed(User $user): bool
  {
    foreach ($this->participants as $participant) {
      if ($participant->user_id === $user->id)
        return true;
    }
    return false;
  }

  public function creator()
  {
    return $this->belongsTo(User::class, 'creator_id');
  }

  public function expenseType()
  {
    return $this->belongsTo(ExpenseType::class, 'expense_type');
  }

  public function activity()
  {
    return $this->belongsTo(Activity::class, 'activity_id');
  }

  public function payments()
  {
    return $this->hasMany(Payment::class, 'expense_id');
  }

  public function canBeConfirmedBy(User $user): bool
  {
    /** @var ExpenseParticipant $participant */
    $participant = $this->participants()->where('user_id', $user->id)->first();
    if ($participant === null) return false;
    return $participant->state !== ExpenseParticipant::STATUS_CONFIRMED;
  }

  public function canBeDisputedBy(User $user): bool
  {
    /** @var ExpenseParticipant $participant */
    $participant = $this->participants()->where('user_id', $user->id)->first();
    if ($participant === null) return false;
    return $participant->state === ExpenseParticipant::STATUS_WAITING_VALIDATION;
  }

  public function quotePart(User $user): float
  {
    /** @var ExpenseParticipant $participant */
    $participant = $this->participants()->where('user_id', $user->id)->first();
    if ($participant === null) return 0.0;
    return $participant->amount;
  }

  public function isDisputed(): bool
  {
    return $this->participants()->where('state', ExpenseParticipant::STATUS_DISPUTED)->exists();
  }

  public function isConfirmed(): bool
  {
    return !$this->participants()->where('state', '!=', ExpenseParticipant::STATUS_CONFIRMED)->exists();
  }
}
