<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment
 * @package App\Models
 * @property int id
 * @property string payment_date
 * @property float amount
 * @property int payer_id
 * @property int expense_id
 * @property string created_at
 * @property string updated_at
 * @property Expense expense
 * @mixin Builder
 */
class Payment extends Model
{
  protected $table = 'payments';

  protected $guarded = ['id'];

  public function expense()
  {
    return $this->belongsTo(Expense::class, 'expense_id');
  }

  public function user()
  {
    return $this->belongsTo(User::class, 'payer_id');
  }
}
