<?php

namespace App\Models;

use App\Models\Interfaces\Disputable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Transfer
 * @package App\Models
 * @mixin Builder
 * @property int id
 * @property int state
 * @property float amount
 * @property string description
 * @property int creator_id
 * @property int from_user_id
 * @property int to_user_id
 * @property string value_date
 * @property string created_at
 * @property string updated_at
 * @property User receiver
 * @property User sender
 */
class Transfer extends Model implements Disputable
{
  protected $table = 'transfers';

  public const STATUS_WAITING = 0;
  public const STATUS_CONFIRMED = 5;
  public const STATUS_DISPUTED = 10;

  protected $fillable = [
      'state',
      'description',
      'creator_id',
      'from_user_id',
      'to_user_id',
      'amount',
      'value_date'
  ];

  /**
   * Get the user that received the transfer
   */
  public function creator()
  {
    return $this->belongsTo(User::class, 'creator_id');
  }

  /**
   * Get the user that received the transfer
   */
  public function receiver()
  {
    return $this->belongsTo(User::class, 'to_user_id');
  }

  /**
   * Get the user that sent the transfer
   */
  public function sender()
  {
    return $this->belongsTo(User::class, 'from_user_id');
  }

  public function scopeOnlyConfirmed(Builder $query): Builder
  {
    return $query->where('state', Transfer::STATUS_CONFIRMED);
  }

  public function scopeWaiting(Builder $query): Builder
  {
    return $query->where('state', Transfer::STATUS_WAITING);
  }

  public function scopeBetweenUsers(Builder $query, User $user1, User $user2)
  {
    return $query->where(function (Builder $query) use ($user1, $user2) {
      $query->where('from_user_id', $user1->id)->where('to_user_id', $user2->id);
    })->orWhere(function (Builder $query) use ($user1, $user2) {
      $query->where('from_user_id', $user2->id)->where('to_user_id', $user1->id);
    });
  }

  public function getTextualStatus(): string
  {
    $res = [
        self::STATUS_WAITING => ucfirst(__('misc.pending')),
        self::STATUS_CONFIRMED => ucfirst(__('misc.confirmed')),
        self::STATUS_DISPUTED => ucfirst(__('misc.disputed')) . '!',
    ];
    return array_key_exists($this->state, $res) ? $res[$this->state] : 'Inconnu';
  }

  public function getStatusBadge($customClass = ''): string
  {
    $res = [
        self::STATUS_WAITING => 'bg-secondary',
        self::STATUS_CONFIRMED => 'bg-primary',
        self::STATUS_DISPUTED => 'bg-danger',
    ];
    return array_key_exists($this->state, $res)
        ? "<span class='badge rounded-pill {$res[$this->state]} {$customClass}'>{$this->getTextualStatus()}</span>"
        : 'Inconnu';
  }

  public function isReceived(): bool
  {
    return Auth::user()->id === $this->receiver->id;
  }

  public function isSent(): bool
  {
    return !$this->isReceived();
  }

  public function activity()
  {
    return $this->belongsTo(Activity::class, 'activity_id');
  }

  public function isUserAllowed(User $user)
  {
    return $user->id === $this->to_user_id || $user->id === $this->from_user_id;
  }

  public function isDisputed(): bool
  {
    return $this->state === self::STATUS_DISPUTED;
  }

  public function isConfirmed(): bool
  {
    return $this->state === self::STATUS_CONFIRMED;
  }

  public function canBeConfirmedBy(User $user): bool
  {
    return !$this->isConfirmed() && $this->receiver->id === $user->id;
  }

  public function canBeDisputedBy(User $user): bool
  {
    return !$this->isDisputed() && !$this->isConfirmed() && $this->receiver->id === $user->id;
  }

}
