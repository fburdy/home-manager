<?php

namespace App\Models\Interfaces;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

interface Disputable
{
  public function isDisputed(): bool;

  public function isConfirmed(): bool;

  public function canBeConfirmedBy(User $user): bool;

  public function canBeDisputedBy(User $user): bool;

  public function scopeOnlyConfirmed(Builder $query): Builder;
}
