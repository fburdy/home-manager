<?php

namespace App\Models;

use App\Utils\Utils;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivityUser
 * @package App\Models
 * @property int id
 * @property int user_id
 * @property int activity_id
 * @property string validation_date
 * @property string invitation_token
 * @property int role
 * @property string created_at
 * @property string updated_at
 * @mixin Builder
 */
class ActivityUser extends Model
{
  protected $table = 'activity_users';

  protected $guarded = ['id'];

  public function activity()
  {
    return $this->belongsTo(Activity::class, 'activity_id');
  }

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function isValidated(): bool
  {
    return $this->validation_date !== null;
  }

  public function isAdmin(): bool
  {
    return $this->role === User::ROLE_ADMIN;
  }

  public function scopeWaitingValidation(Builder $query): Builder
  {
    return $query->whereNull('validation_date');
  }

  public function scopeValidated(Builder $query): Builder
  {
    return $query->whereNotNull('validation_date');
  }
}
