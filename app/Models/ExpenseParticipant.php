<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ExpenseParticipant
 * @package App\Models
 * @property int id
 * @property float amount
 * @property int state
 * @property int user_id
 * @property int expense_id
 * @property string created_at
 * @property string updated_at
 * @property Expense expense
 * @property User user
 * @mixin Builder
 */
class ExpenseParticipant extends Model
{
  public const STATUS_WAITING_VALIDATION = 0;
  public const STATUS_CONFIRMED = 5;
  public const STATUS_DISPUTED = 10;

  protected $table = 'expenses_participants';

  protected $guarded = ['id'];

  /**
   * Get the related expense
   */
  public function expense()
  {
    return $this->belongsTo(Expense::class, 'expense_id');
  }

  /**
   * Get the related expense
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function scopeWaiting(Builder $query): Builder
  {
    return $query->where('state', ExpenseParticipant::STATUS_WAITING_VALIDATION);
  }
}
