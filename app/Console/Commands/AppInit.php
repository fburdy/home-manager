<?php

namespace App\Console\Commands;

use App\Models\User;
use Database\Seeders\ActivityTableSeeder;
use Database\Seeders\ExpensesSeeder;
use Database\Seeders\ExpenseTypesTableSeeder;
use Database\Seeders\OrganizationsTableSeeder;
use Database\Seeders\TransfersTableSeeder;
use Database\Seeders\UsersTableSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class AppInit extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:init {--d|dev : Run dev/test seeders} {--f|force : Force database purge end seeds}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    if (!$this->option('force')) {
      $this->alert('THIS COMMAND WILL PURGE DATABASE. DO NOT USE THIS IN PRODUCTION !');
      if (!$this->confirm('Do you wish to continue?')) {
        $this->info('Operation stopped. Bye.');
        return 1;
      }
    }
    $this->call('migrate:fresh');
    $this->comment('Applying production seeders');
    $this->call('db:seed', ['--class' => ExpenseTypesTableSeeder::class]);
    if (!$this->option('dev')) {
      $this->promptFirstProdUser();
    } else {
      $this->call('db:seed', ['--class' => UsersTableSeeder::class]);
      $this->call('db:seed', ['--class' => ActivityTableSeeder::class]);
      $this->call('db:seed', ['--class' => TransfersTableSeeder::class]);
      $this->call('db:seed', ['--class' => OrganizationsTableSeeder::class]);
      $this->call('db:seed', ['--class' => ExpenseTypesTableSeeder::class]);
      $this->call('db:seed', ['--class' => ExpensesSeeder::class]);
    }

    $this->info('Done. Bye.');
    return 0;
  }

  private function promptFirstProdUser()
  {
    $email = null;
    while (empty($email)) { // todo: check email validity
      $email = $this->ask('First user email address?');
    }
    $user = new User([
        'email' => $email,
        'name' => $this->ask('First user full name?'),
        'password' => Hash::make($this->secret('First user password?')),
    ]);
    $user->save();
    $this->info("First user created with email address {$email}. Use it to sign in the web server.");
    return $user;
  }
}
