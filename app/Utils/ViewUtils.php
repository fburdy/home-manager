<?php

namespace App\Utils;

use App\Models\Expense;
use App\Models\ExpenseParticipant;

class ViewUtils
{
  public static function participantStatusBadge(ExpenseParticipant $participant)
  {
    $badges = [
        ExpenseParticipant::STATUS_WAITING_VALIDATION => '<span class="badge rounded-pill bg-secondary">' . __('misc.pending') . '</span>',
        ExpenseParticipant::STATUS_CONFIRMED => '<span class="badge rounded-pill bg-primary">' . __('misc.confirmed') . '</span>',
        ExpenseParticipant::STATUS_DISPUTED => '<span class="badge rounded-pill bg-danger">' . __('misc.disputed') . '</span>',
    ];
    return $badges[$participant->state] ?? '<span class="badge rounded-pill bg-light">Inconnu</span>';
  }

  public static function expenseStatusBadge(Expense $expense, $customClass = '')
  {
    $waitingParticipants = 0;
    foreach ($expense->participants()->get() as $participant) {
      if ($participant->state === ExpenseParticipant::STATUS_DISPUTED) {
        return "<span class='badge rounded-pill bg-danger {$customClass}'>" . __('misc.disputed') . "</span>";
      }
      if ($participant->state === ExpenseParticipant::STATUS_WAITING_VALIDATION) {
        $waitingParticipants++;
      }
    }
    if ($waitingParticipants > 0) {
      return "<span class='badge rounded-pill bg-secondary {$customClass}'>" . __('misc.nb.pending', ['nb' => $waitingParticipants]) . "</span>";
    }
    return "<span class='badge rounded-pill bg-primary {$customClass}'>" . __('misc.confirmed') . "</span>";
  }

  public static function randomActivityName(): string
  {
    $types = [
        'fr' => [
            'Vacances ' . ViewUtils::randomLocation(),
            'Vacances ' . ViewUtils::randomLocation(),
            'Voyage ' . ViewUtils::randomLocation(),
            'Voyage ' . ViewUtils::randomLocation(),
            'Séjour ' . ViewUtils::randomLocation(),
            'Séjour ' . ViewUtils::randomLocation(),
            'Travaux de rénovation',
            'Mon projet d\'avenir',
            'Le projet secret',
            'Sorties en ville',
            'Séjour en famille',
            'Copropriété',
            'Collocation',
        ],
        'en' => [
            'Trip ' . ViewUtils::randomLocation(),
            'Trip ' . ViewUtils::randomLocation(),
            'Journey ' . ViewUtils::randomLocation(),
            'Journey ' . ViewUtils::randomLocation(),
            'Travel ' . ViewUtils::randomLocation(),
            'Travel ' . ViewUtils::randomLocation(),
            'Renovation works',
            'My future project',
            'My secret project',
            'Dinner out',
        ]

    ];

    $locale = app()->getLocale();
    return $types[$locale][array_rand(array_unique($types[$locale]))];
  }

  public static function randomLocation(): string
  {
    $types = [
        'fr' => [
            'en France',
            'en Espagne',
            'aux USA',
            'en Chine',
            'à Paris',
            'à Londres',
            'à Saint-Tropez',
            'à New-York',
            'en Catalogne',
            'au Japon',
            'au Brésil',
        ],
        'en' => [
          'to France',
          'to Spain',
          'to the USA',
          'to China',
          'to Paris',
          'to London',
          'to Saint-Tropez',
          'to NYC',
          'to Catalonia',
          'to Japan',
          'to Bresil',
        ]

    ];
    $locale = app()->getLocale();

    return $types[$locale][array_rand($types[$locale])];
  }
}
