<?php

namespace App\Utils;

use Carbon\Carbon;

class Utils
{

  public static function formatDate($date, $short = false)
  {
    $locale = app()->getLocale();
    if ($locale === 'fr') {
      return Carbon::parse($date)->format($short ? 'd/m' : 'd/m/Y');
    }

    return Carbon::parse($date)->format('m-d-Y');
  }

  public static function relativeDateFrench($date, $short = false)
  {
    $date = Carbon::parse($date);
    if ($date->isToday()) {
      return 'Aujourd\'hui';
    }
    if ($date->isYesterday()) {
      return 'Hier';
    }
    if ($date->isCurrentYear()) {
      return $date->translatedFormat('jS F');
    }
    return $date->translatedFormat('jS F Y');
  }

  /**
   * Return an amount, money formatted
   * @param string|float|int $amount
   * @param string $currency
   * @param int $decimals
   * @return string
   */
  public static function asPrice($amount, string $currency = '€', int $decimals = 2): string
  {
    if (is_string($amount)) {
      $amount = str_replace([' ', '\''], '', $amount);
    }
    if (app()->getLocale() !== 'fr') {
      $minus = $amount < 0.0 ? '-' : '';
      $val = number_format((float) abs($amount), $decimals, '.', '\'');
      return "{$minus}{$currency}{$val}";
    }
    $val = number_format((float) $amount, $decimals, ',', ' ');
    return "{$val} {$currency}";
  }

  public static function truncate(?string $str = '', int $width = 0, $marker = '.'): string
  {
    return trim(mb_strimwidth(preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', $str), 0, $width, $marker));
  }

  /**
   * @param int $length
   * @param string $prefix
   * @param string $suffix
   * @return string
   * @throws \Exception
   */
  public static function randomToken($length = 60, $prefix = '', $suffix = ''): string
  {
    $token = '';
    $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $max = strlen($alphabet);
    for ($i = 0; $i < $length; $i++) {
      $token .= $alphabet[random_int(0, $max - 1)];
    }
    if (!empty($prefix)) {
      $token = trim($prefix) . '_' . $token;
    }
    if (!empty($suffix)) {
      $token .= '_' . trim($suffix);
    }
    return $token;
  }
}
