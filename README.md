Home Manager
============

![](https://gitlab.com/fburdy/home-manager/-/raw/develop/resources/img/screenshots/logo-banner.png)

**Home Manager is an opensource & self-hosted Tricount alternative.**

Manage your expenses, split your bills with your friends, roommates or family members.

The app does the calculation for you in complete transparency to see who owes whom.

![](https://gitlab.com/fburdy/home-manager/-/raw/develop/resources/img/screenshots/heading_shots_en.png)

# Key features

See [some screenshots](https://gitlab.com/fburdy/home-manager/-/tree/develop/resources/img/screenshots) of the main interfaces.

> **French & English support**

> **User registration and authentication**

> **Make a group of participants**

> **Add your expenses and point out who is contributing**

> **Each of those concerned by an expense can see the details and can confirm (or maybe dispute)**

> **Check your balance of who owes whom at any time**

> **Reimburse or advance money to you fellow group members**

# Build & Run

#### Requirements

The web app is based on a Laravel server and a SQL database. The front-end is based on Bootstrap.

- PHP 8.1.0 or greater
- A MySQL, MariaDB, PostgreSQL, SQL Server or SQLite database
- Composer & Yarn

## Install

#### Step 1: Download the sources

```shell script
git clone git@gitlab.com:francoisburdy/home-manager.git
cd home-manager
```

#### Step 2: Create an SQL database

Create a database (MySQL, MariaDB, PostgreSQL, SQL Server or even SQLite)

#### Step 3: Set your env variables

You need to set a few env variables to make the server work as you want it to.

For a development local env, you can fulfil variables in `.env` file.

#### Step 4: Install dependencies & build front-end

```shell script
# Install back-end dependencies
composer install

# Generate an app secret key
php artisan key:generate

# Install front-end dependencies
yarn install

# Build front-end with webpack (see package.json for more options)
yarn production
# yarn dev or yarn watch to development purposes
```

#### Step 5: Initialize database structure

```shell script
# Initialize database structure migrations
yarn fresh 
# you can also load demo/testing data seeders with this command:
# yarn fresh-seed 
```

That's it. You're all set.

## Run

In order to expose a production server, using Nginx for example, you should follow the
official [deployment guidelines](https://laravel.com/docs/9.x/deployment)
of the Laravel framework.

If you just want to run a local development server, you can run it this way:

```shell script
# Run a dev server on localhost:8000
php artisan serve
```

## License

Home Manager is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
