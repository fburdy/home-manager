<?php

namespace Tests\Unit;

use App\Utils\Utils;
use Tests\TestCase;

class UtilsTest extends TestCase
{
  /**
   * A basic test example.
   *
   * @return void
   */
  public function testAsPrice()
  {
    $this->assertEquals('0,00 €', Utils::asPrice(0));
    $this->assertEquals('1 020,39 €', Utils::asPrice(1020.39));
    $this->assertEquals('1 020,39 €', Utils::asPrice('1020.390'));
    $this->assertEquals('-10 086 865 $', Utils::asPrice(-10086865.06, '$', 0));
  }
}
